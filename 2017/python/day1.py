#!/usr/bin/env python3

from utils import test

examples_part1 = {
    '1122': 3,
    '1111': 4,
    '1234': 0,
    '91212129': 9,
}

examples_part2 = {
    '1212': 6,
    '1221': 0,
    '123425': 4,
    '123123': 12,
    '12131415': 4,
}

INPUT = 'resources/day1.txt'


def solve_part1(ipt):
    numbers = [line for line in ipt.split(', ')][0]
    numbers += numbers[0]
    counter = 0
    for i in range(len(numbers) - 1):
        if numbers[i] == numbers[i+1]:
            counter += int(numbers[i])
    return counter

def solve_part2(ipt):
    numbers = [line for line in ipt.split(', ')][0]
    counter = 0
    for i in range(len(numbers)):
        a = numbers[i]
        b = numbers[(i+(len(numbers)//2))%len(numbers)]
        if a == b:
            counter += int(numbers[i])
    return counter


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    sum_duplicates = solve_part1(ipt)
    print(sum_duplicates)
    sum_duplicates = solve_part2(ipt)
    print(sum_duplicates)