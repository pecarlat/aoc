#!/usr/bin/env python3

from utils import test

examples_part1 = {
    '{}': 1,
    '{{{}}}': 6,
    '{{},{}}': 5,
    '{{{},{},{{}}}}': 16,
    '{<a>,<a>,<a>,<a>}': 1,
    '{{<ab>},{<ab>},{<ab>},{<ab>}}': 9,
    '{{<!!>},{<!!>},{<!!>},{<!!>}}': 9,
    '{{<a!>},{<a!>},{<a!>},{<ab>}}': 3,
}

examples_part2 = {
    '<>': 0,
    '<random characters>': 17,
    '<<<<>': 3,
    '<{!>}>': 2,
    '<!!>': 0,
    '<!!!>>': 0,
    '<{o"i!a,<{i<a>': 10,
}

INPUT = 'resources/day9.txt'


def remove_exceptions(s):
    chars = ''
    skip = False
    for i in range(len(s)):
        if skip:
            skip = False
        elif s[i] == '!':
            skip = True
        else:
            chars += s[i]
    return chars

def remove_garbages(s):
    chars = ''
    skip = False
    for c in s:
        if skip:
            skip = (c != '>')
        elif c == '<':
            skip = True
        else:
            chars += c
    return chars

def count_garbages(s):
    nb_skipped = 0
    skip = False
    for c in s:
        if skip:
            skip = (c != '>')
            nb_skipped += int(skip)
        elif c == '<':
            skip = True
    return nb_skipped

def solve_part1(ipt):
    chars = remove_exceptions(ipt)
    chars = remove_garbages(chars)

    level = 1
    total_score = 0
    for c in chars:
        if c == '{':
            level += 1
        elif c == '}':
            level -= 1
            total_score += level
    return total_score

def solve_part2(ipt):
    chars = remove_exceptions(ipt)
    return count_garbages(chars)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    score = solve_part1(ipt)
    print(score)
    score = solve_part2(ipt)
    print(score)