#!/usr/bin/env python3

from utils import test

examples_part1 = {
    ('0 <-> 2\n'
     '1 <-> 1\n'
     '2 <-> 0, 3, 4\n'
     '3 <-> 2, 4\n'
     '4 <-> 2, 3, 6\n'
     '5 <-> 6\n'
     '6 <-> 4, 5'): 6,
}

examples_part2 = {
    ('0 <-> 2\n'
     '1 <-> 1\n'
     '2 <-> 0, 3, 4\n'
     '3 <-> 2, 4\n'
     '4 <-> 2, 3, 6\n'
     '5 <-> 6\n'
     '6 <-> 4, 5'): 2,
}

INPUT = 'resources/day12.txt'


def find_group_of(graph, root):
    pipe = [root]
    seen = set()
    while pipe:
        curr = pipe.pop()
        seen.add(curr)
        pipe += [g for g in graph[curr] if not (g in seen or g in pipe)]
    return seen

def solve_part1(ipt):
    lines = [l.split(' <-> ') for l in ipt.splitlines()]
    graph = {n: c.split(', ') for n, c in lines}
    return len(find_group_of(graph, '0'))

def solve_part2(ipt):
    lines = [l.split(' <-> ') for l in ipt.splitlines()]
    graph = {n: c.split(', ') for n, c in lines}
    unseen = list(graph.keys())
    nb_groups = 0
    while unseen:
        nb_groups += 1
        seen = find_group_of(graph, unseen[0])
        for s in seen:
            unseen.remove(s)

    return nb_groups


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    shortest = solve_part1(ipt)
    print(shortest)
    furthest = solve_part2(ipt)
    print(furthest)