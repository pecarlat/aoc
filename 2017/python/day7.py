#!/usr/bin/env python3

import collections
import networkx as nx

from utils import test

examples_part1 = {
    ('pbga (66)\n'
     'xhth (57)\n'
     'ebii (61)\n'
     'havc (66)\n'
     'ktlj (57)\n'
     'fwft (72) -> ktlj, cntj, xhth\n'
     'qoyq (66)\n'
     'padx (45) -> pbga, havc, qoyq\n'
     'tknk (41) -> ugml, padx, fwft\n'
     'jptl (61)\n'
     'ugml (68) -> gyxo, ebii, jptl\n'
     'gyxo (61)\n'
     'cntj (57)'): 'tknk',
}

examples_part2 = {
}

INPUT = 'resources/day7.txt'


def build_graph(lines):
    graph = nx.DiGraph()

    for line in lines:
        graph.add_node(line[0], weight=int(line[1]))
        if '->' in line:
            for child in [n for n in line[3:]]:
                graph.add_edge(line[0], child)

    return graph

def solve_part1(ipt):
    lines = [[n.strip('(),') for n in l.split()] for l in ipt.splitlines()]
    graph = build_graph(lines)

    # Topological sort to find the root of the tree
    ordered = list(nx.topological_sort(graph))

    return ordered[0]

def solve_part2(ipt):
    lines = [[n.strip('(),') for n in l.split()] for l in ipt.splitlines()]
    graph = build_graph(lines)

    # Topological sort to find the root of the tree
    ordered = list(nx.topological_sort(graph))

    weights = {}
    for node in reversed(ordered):
        total = graph.nodes[node]['weight']
        counts = collections.Counter(weights[child] for child in graph[node])

        unbalanced = None
        for child in graph[node]:
            if len(counts) > 1 and counts[weights[child]] == 1:
                unbalanced = child
                break

            val = weights[child]
            total += weights[child]

        if unbalanced:
            diff = weights[unbalanced] - val
            return graph.nodes[unbalanced]['weight'] - diff

        weights[node] = total

    return 0


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    name = solve_part1(ipt)
    print(name)
    weight = solve_part2(ipt)
    print(weight)