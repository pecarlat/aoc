def test(name, d, func):
    for k, val in d.items():
        result = func(k)
        assert result == val, \
               'Problem with ' + k + ': ' + str(result) + ' /= ' + str(val)
    print("Test " + name + ": All success.")