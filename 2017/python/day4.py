#!/usr/bin/env python3

from utils import test

examples_part1 = {
    'aa bb cc dd ee': 1,
    'aa bb cc dd aa': 0,
    'aa bb cc dd aaa': 1,
}

examples_part2 = {
    'abcde fghij': 1,
    'abcde xyz ecdab': 0,
    'a ab abc abd abf abj': 1,
    'iiii oiii ooii oooi oooo': 1,
    'oiii ioii iioi iiio': 0,
}

INPUT = 'resources/day4.txt'


def sort_str(s):
    return ''.join(sorted(s))

def solve_part1(ipt):
    lines = [line.split() for line in ipt.splitlines()]
    return sum([len(line) == len(set(line)) for line in lines])

def solve_part2(ipt):
    lines = [[sort_str(l) for l in line.split()] for line in ipt.splitlines()]
    return sum([len(line) == len(set(line)) for line in lines])


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_valids = solve_part1(ipt)
    print(nb_valids)
    nb_valids = solve_part2(ipt)
    print(nb_valids)