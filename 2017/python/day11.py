#!/usr/bin/env python3

from collections import defaultdict

from utils import test

examples_part1 = {
    'ne,ne,ne': 3,
    'ne,ne,sw,sw': 0,
    'ne,ne,s,s': 2,
    'se,sw,se,sw,sw': 3,
}

examples_part2 = {
}

INPUT = 'resources/day11.txt'


def purge_directions(dirs):
    for u, d in zip(['n', 'ne', 'e', 'se'], ['s', 'sw', 'w', 'nw']):
        diff = min(dirs[u], dirs[d])
        dirs[u] -= diff
        dirs[d] -= diff
    return dirs

def divide_directions(dirs):
    for d in ['ne', 'se', 'sw', 'nw']:
        for sub_d in d:
            dirs[sub_d] += dirs[d] // 2
        dirs[d] = dirs[d] % 2
    return dirs

def update_pos(pos, direction):
    return {
        'n':  [pos[0] - 1,   pos[1]],
        'ne': [pos[0] - 0.5, pos[1] + 0.5],
        'e':  [pos[0],       pos[1] + 1],
        'se': [pos[0] + 0.5, pos[1] + 0.5],
        's':  [pos[0] + 1,   pos[1]],
        'sw': [pos[0] + 0.5, pos[1] - 0.5],
        'w':  [pos[0],       pos[1] - 1],
        'nw': [pos[0] - 0.5, pos[1] - 0.5],
    }[direction]

def solve_part1(ipt):
    directions = ipt.split(',')
    directions = defaultdict(int, {i: directions.count(i) for i in directions})
    directions = divide_directions(directions)
    directions = purge_directions(directions)
    return sum(directions.values())

def solve_part2(ipt):
    directions = ipt.split(',')
    max_distance = 0
    pos = [0, 0]
    for direction in directions:
        pos = update_pos(pos, direction)
        max_distance = max(max_distance, sum(map(abs, pos)))
    return max_distance


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    shortest = solve_part1(ipt)
    print(shortest)
    furthest = solve_part2(ipt)
    print(furthest)