#!/usr/bin/env python3

from utils import test

examples_part1 = {
    '0\n3\n0\n1\n-3': 5,
}

examples_part2 = {
    '0\n3\n0\n1\n-3': 10,
}

INPUT = 'resources/day5.txt'


def solve_part1(ipt):
    jumps = [int(line) for line in ipt.splitlines()]
    idx = 0
    count = 0
    while 0 <= idx < len(jumps):
        old_idx = idx
        idx += jumps[idx]
        jumps[old_idx] += 1
        count += 1
    return count

def solve_part2(ipt):
    jumps = [int(line) for line in ipt.splitlines()]
    idx = 0
    count = 0
    while 0 <= idx < len(jumps):
        old_idx = idx
        idx += jumps[idx]
        if jumps[old_idx] < 3:
            jumps[old_idx] += 1
        else:
            jumps[old_idx] -= 1
        count += 1
    return count


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_steps = solve_part1(ipt)
    print(nb_steps)
    nb_steps = solve_part2(ipt)
    print(nb_steps)