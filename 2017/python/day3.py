#!/usr/bin/env python3

import math

from utils import test

examples_part1 = {
    '1': 0,
    '12': 3,
    '23': 2,
    '1024': 31,
}

examples_part2 = {
}

INPUT = 'resources/day3.txt'


def next_coord(x, y):
    if -x >= y and x >= y: return (x+1, y)
    if -x >= y and x < y:  return (x, y-1)
    if -x < y  and x <= y: return (x-1, y)
    if -x < y  and x > y:  return (x, y+1)

def solve_part1(ipt):
    start = int(ipt)
    up_sq = math.ceil(math.sqrt(start))
    sq_nb = up_sq // 2
    down, up = pow(up_sq - 1, 2), pow(up_sq, 2)
    diff = up - down
    return sq_nb + abs(sq_nb - abs((diff // 2) + 1 - (diff - (up - start))))

def solve_part2(ipt):
    x, y = 0, 0
    vals = {(x, y): 1}
    neighbors = [(i, j) for i in [-1, 0, 1] for j in [-1, 0, 1]]

    while vals[(x, y)] < int(ipt):
        x, y = next_coord(x, y)
        vals[(x, y)] = sum(vals.get((x + i, y + j), 0) for i, j in neighbors)
    return vals[(x, y)]


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    smallest = solve_part1(ipt)
    print(smallest)
    smallest = solve_part2(ipt)
    print(smallest)