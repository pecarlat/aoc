#!/usr/bin/env python3

from functools import reduce

from utils import test

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day10.txt'
VAL_SIZE = 256


def get_portion(l, i, s):
    if i + s < len(l):
        return l[i:i+s]
    else:
        return l[i:] + l[:(s - len(l) + i)]

def replace(l, seq, i):
    for j, x in enumerate(seq):
        l[(i + j) % len(l)] = x
    return l

def apply_sizes(numbers, seq_sizes, skip_size, idx):
    for size in seq_sizes:
        seq = get_portion(numbers, idx, size)
        numbers = replace(numbers, seq[::-1], idx)
        idx = (idx + size + skip_size) % len(numbers)
        skip_size += 1

    return numbers, skip_size, idx

def solve_part1(ipt):
    numbers = list(range(VAL_SIZE))
    sizes =[int(x) for x in ipt.split(',')]
    skip_size = 0
    idx = 0
    return apply_sizes(numbers, sizes, skip_size, idx)[0]

def solve_part2(ipt):
    numbers = list(range(VAL_SIZE))
    sizes = [ord(x) for x in ipt] + [17, 31, 73, 47, 23]
    skip_size = 0
    idx = 0
    for _ in range(64):
        numbers, skip_size, idx = apply_sizes(numbers, sizes, skip_size, idx)

    dense = []
    for x in range(16):
        subslice = numbers[16*x:16*x+16]
        dense.append('%02x' % reduce((lambda x, y: x ^ y),subslice))
    return ''.join(dense)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    numbers = solve_part1(ipt)
    print(numbers[0] * numbers[1])
    hsh = solve_part2(ipt)
    print(hsh)