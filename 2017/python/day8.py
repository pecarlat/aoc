#!/usr/bin/env python3

from collections import defaultdict

from utils import test

examples_part1 = {
    ('b inc 5 if a > 1\n'
     'a inc 1 if b < 5\n'
     'c dec -10 if a >= 1\n'
     'c inc -20 if c == 10'): 1,
}

examples_part2 = {
    ('b inc 5 if a > 1\n'
     'a inc 1 if b < 5\n'
     'c dec -10 if a >= 1\n'
     'c inc -20 if c == 10'): 10,
}

INPUT = 'resources/day8.txt'


def condition(r, o, n):
    return {
        '==': r == n,
        '>':  r >  n,
        '<':  r <  n,
        '>=': r >= n,
        '<=': r <= n,
        '!=': r != n,
    }[o]

def operation(r, o, n):
    return {
        'inc': r + n,
        'dec': r - n,
    }[o]

def solve_part1(ipt):
    registers = defaultdict(int)
    for line in ipt.splitlines():
        reg, op, nb, _, c_reg, c_op, c_nb = line.split()
        if condition(registers[c_reg], c_op, int(c_nb)):
            registers[reg] = operation(registers[reg], op, int(nb))

    return max(registers.values())

def solve_part2(ipt):
    max_value = 0

    registers = defaultdict(int)
    for line in ipt.splitlines():
        reg, op, nb, _, c_reg, c_op, c_nb = line.split()
        if condition(registers[c_reg], c_op, int(c_nb)):
            registers[reg] = operation(registers[reg], op, int(nb))
            max_value = max(max_value, registers[reg])

    return max_value


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    highest = solve_part1(ipt)
    print(highest)
    highest = solve_part2(ipt)
    print(highest)