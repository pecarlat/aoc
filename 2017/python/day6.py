#!/usr/bin/env python3

from utils import test

examples_part1 = {
    '0\t2\t7\t0': 5,
}

examples_part2 = {
    '0\t2\t7\t0': 4,
}

INPUT = 'resources/day6.txt'


def hash_me(l):
    return '_'.join([str(e) for e in l])

def solve_part1(ipt):
    banks = [int(nb) for nb in ipt.split()]
    configs = [hash_me(banks)]
    count = 0
    while len(configs) == len(set(configs)):
        curr_max = max(banks)
        idx = banks.index(curr_max)
        banks[idx] = 0
        for i in range(idx + 1, idx + 1 + curr_max):
            banks[i % len(banks)] += 1
        configs.append(hash_me(banks))
        count += 1
    return count

def solve_part2(ipt):
    banks = [int(nb) for nb in ipt.split()]
    configs = [hash_me(banks)]
    while configs[-1] not in configs[:-1]:
        curr_max = max(banks)
        idx = banks.index(curr_max)
        banks[idx] = 0
        for i in range(idx + 1, idx + 1 + curr_max):
            banks[i % len(banks)] += 1
        configs.append(hash_me(banks))
    return len(configs) - 1 - configs.index(configs[-1])


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_steps = solve_part1(ipt)
    print(nb_steps)
    loop_size = solve_part2(ipt)
    print(loop_size)