#!/usr/bin/env python3

from utils import test

examples_part1 = {
    ('5\t1\t9\t5\n'
     '7\t5\t3\n'
     '2\t4\t6\t8'): 18,
}

examples_part2 = {
    ('5\t9\t2\t8\n'
     '9\t4\t7\t3\n'
     '3\t8\t6\t5'): 9,
}

INPUT = 'resources/day2.txt'


def even_division(l):
    l = sorted(l)
    for i in range(len(l)):
        for j in range(i + 1, len(l)):
            if l[j] / l[i] == l[j] // l[i]:
                return l[j] // l[i]
    return 0

def solve_part1(ipt):
    lines = [[int(x) for x in line.split('\t')] for line in ipt.splitlines()]
    std = lambda x: max(x) - min(x)
    return sum(map(std, lines))

def solve_part2(ipt):
    lines = [[int(x) for x in line.split('\t')] for line in ipt.splitlines()]
    return sum(map(even_division, lines))


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    sum_checksum = solve_part1(ipt)
    print(sum_checksum)
    sum_checksum = solve_part2(ipt)
    print(sum_checksum)