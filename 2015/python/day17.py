#!/usr/bin/env python3

from utils import test

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day17.txt'
NB_LITERS = 150


def solve_part1(ipt):
    nb_options = 0
    all_containers = [int(e) for e in ipt.split('\n')]
    for mask in range(1 << len(all_containers)):
        masked = [all_containers[::-1][i]
                  for i, m in enumerate(bin(mask)[2:][::-1])
                  if bool(int(m))]
        if sum(masked) == NB_LITERS:
            nb_options += 1
    return nb_options

def solve_part2(ipt):
    nb_containers = []
    all_containers = [int(e) for e in ipt.split('\n')]
    for mask in range(1 << len(all_containers)):
        masked = [all_containers[::-1][i]
                  for i, m in enumerate(bin(mask)[2:][::-1])
                  if bool(int(m))]
        if sum(masked) == NB_LITERS:
            nb_containers.append(bin(mask).count('1'))
    return nb_containers.count(min(nb_containers))


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_combin = solve_part1(ipt)
    print(nb_combin)
    nb_combin = solve_part2(ipt)
    print(nb_combin)