#!/usr/bin/env python3

from utils import test

import re

examples_part1 = {
    'abcdefgh': 'abcdffaa',
}

examples_part2 = {
}

INPUT = 'resources/day11.txt'

def letter_dec(c):
    return chr(ord(c) - 1)

def letter_inc(c):
    return chr(ord(c) + 1)

def first_non_matching(l, x):
    return next((i for i, v in enumerate(l) if v != x), -1)

def increment_password(password):
    idx_not_max = len(password) - first_non_matching(password[::-1], 'z') - 1
    while True:
        password[idx_not_max] = letter_inc(password[idx_not_max])
        if password[idx_not_max] not in ['i', 'o', 'l']:
            break
    for i in range(idx_not_max + 1, len(password)):
        password[i] = 'a'
    return password

def is_valid(s):
    s = ''.join(s)
    has_double_pairs = len(re.findall(r'([a-z])\1', s)) >= 2
    following = False
    for i in range(1, len(s) - 1):
        if letter_dec(s[i]) == s[i-1] and letter_inc(s[i]) == s[i+1]:
            following = True

    return has_double_pairs and following

def solve_part1(ipt):
    password = increment_password(list(ipt))

    while not is_valid(password):
        password = increment_password(password)

    return ''.join(password)

def solve_part2(ipt):
    return solve_part1(ipt)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    new_password = solve_part1(ipt)
    print(new_password)
    new_password = solve_part2(new_password)
    print(new_password)