#!/usr/bin/env python3

from utils import test

import re
import numpy as np

examples_part1 = {
    'turn on 0,0 through 999,999': 1000000,
    'toggle 0,0 through 999,0': 1000,
    'turn off 499,499 through 500,500': 0,
}

examples_part2 = {
    'turn on 0,0 through 0,0': 1,
    'toggle 0,0 through 999,999': 2000000,
}

INPUT = 'resources/day6.txt'

def parse_line(line):
    return re.findall("(^[a-zA-Z ]*) (\d+),(\d+) through (\d+),(\d+)", line)[0]

def solve_part1(ipt):
    lights = np.zeros((1000, 1000))
    for line in ipt.split('\n'):
        action, x1, y1, x2, y2 = parse_line(line)
        x1, y1, x2, y2 = int(x1), int(y1), int(x2) + 1, int(y2) + 1
        if action == 'turn on':
            lights[x1:x2,y1:y2] = 1
        elif action == 'turn off':
            lights[x1:x2,y1:y2] = 0
        elif action == 'toggle':
            lights[x1:x2,y1:y2] = ((lights[x1:x2,y1:y2] * 2 - 1) * -1 + 1) / 2

    return int(np.sum(lights))

def solve_part2(ipt):
    lights = np.zeros((1000, 1000))
    for line in ipt.split('\n'):
        action, x1, y1, x2, y2 = parse_line(line)
        x1, y1, x2, y2 = int(x1), int(y1), int(x2) + 1, int(y2) + 1
        if action == 'turn on':
            lights[x1:x2,y1:y2] += 1
        elif action == 'turn off':
            lights[x1:x2,y1:y2] -= 1
        elif action == 'toggle':
            lights[x1:x2,y1:y2] += 2
        lights[lights < 0] = 0

    return int(np.sum(lights))


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_lights_lit = solve_part1(ipt)
    print(nb_lights_lit)
    nb_lights_lit = solve_part2(ipt)
    print(nb_lights_lit)