#!/usr/bin/env python3

from utils import test

examples_part1 = {
    '>': 2,
    '^>v<': 4,
    '^v^v^v^v^v': 2,
}

examples_part2 = {
    '^v': 3,
    '^>v<': 3,
    '^v^v^v^v^v': 11,
}

INPUT = 'resources/day3.txt'

def get_move_direction(character):
    return  {
        '^': (1, 0), 
        '>': (0, 1), 
        'v': (-1, 0), 
        '<': (0, -1),
    }[character]

def solve_part1(ipt):
    houses = []
    house = (0, 0)
    houses.append(house)
    for character in ipt:
        move_direction = get_move_direction(character)
        house = tuple([x + y for x, y in zip(house, move_direction)])
        houses.append(house)
    return len(set(houses))

def solve_part2(ipt):
    houses = []
    santa_house = (0, 0)
    robot_house = (0, 0)
    houses.append(santa_house)
    for character_even, character_odd in zip(ipt[0::2], ipt[1::2]):
        direction_1 = get_move_direction(character_even)
        direction_2 = get_move_direction(character_odd)
        santa_house = tuple([x + y for x, y in zip(santa_house, direction_1)])
        robot_house = tuple([x + y for x, y in zip(robot_house, direction_2)])
        houses.append(santa_house)
        houses.append(robot_house)
    return len(set(houses))


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_houses = solve_part1(ipt)
    print(nb_houses)
    nb_houses = solve_part2(ipt)
    print(nb_houses)