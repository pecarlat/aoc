#!/usr/bin/env python3

from utils import test

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day16.txt'
GOOD_SUE = {
    'children': 3,
    'cats': 7,
    'samoyeds': 2,
    'pomeranians': 3,
    'akitas': 0,
    'vizslas': 0,
    'goldfish': 5,
    'trees': 3,
    'cars': 2,
    'perfumes': 1,
}

def check(c, n, all_equals=True):
    if not all_equals:
        if c in ['trees', 'cats']:
            return n > GOOD_SUE[c]
        elif c in ['pomeranians', 'goldfish']:
            return n < GOOD_SUE[c]
    return n == GOOD_SUE[c]

def solve_part1(ipt):
    for line in ipt.split('\n'):
        _, sue, c1, n1, c2, n2, c3, n3 = line.split()
        chars_names = [c.rstrip(':') for c in [c1, c2, c3]]
        chars_vals = [int(n.rstrip(',')) for n in [n1, n2, n3]]
        if all([check(c, n, all_equals=True)
                for c, n in zip(chars_names, chars_vals)]):
            return sue.rstrip(':')

    return 'No Sue is good enough..'

def solve_part2(ipt):
    for line in ipt.split('\n'):
        _, sue, c1, n1, c2, n2, c3, n3 = line.split()
        chars_names = [c.rstrip(':') for c in [c1, c2, c3]]
        chars_vals = [int(n.rstrip(',')) for n in [n1, n2, n3]]
        if all([check(c, n, all_equals=False)
                for c, n in zip(chars_names, chars_vals)]):
            return sue.rstrip(':')

    return 'No Sue is good enough..'


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    sue_nb = solve_part1(ipt)
    print(sue_nb)
    sue_nb = solve_part2(ipt)
    print(sue_nb)