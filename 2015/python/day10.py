#!/usr/bin/env python3

from utils import test

from itertools import groupby

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day10.txt'

def look_and_say(sequence):
    return ''.join([str(len(list(l))) + e for e, l in groupby(sequence)])

def solve_part1(ipt):
    sequence = ipt
    for i in range(40):
        sequence = look_and_say(sequence)

    return len(sequence)

def solve_part2(ipt):
    sequence = ipt
    for i in range(50):
        sequence = look_and_say(sequence)

    return len(sequence)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    length = solve_part1(ipt)
    print(length)
    length = solve_part2(ipt)
    print(length)