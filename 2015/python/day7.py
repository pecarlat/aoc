#!/usr/bin/env python3

from utils import test

import re

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day7.txt'
pattern_variable = '(\w+) -> (\w+)'
pattern_opposite = 'NOT (\w+) -> (\w+)'
pattern_operator = '(\w+) (AND|OR|LSHIFT|RSHIFT) (\w+) -> (\w+)'

OPERATORS = {
    'AFFECT': lambda args: args[0],
    'NOT':    lambda args: ~args[0],
    'AND':    lambda args: args[0] & args[1],
    'OR':     lambda args: args[0] | args[1],
    'LSHIFT': lambda args: args[0] << args[1],
    'RSHIFT': lambda args: args[0] >> args[1],
}

def parse_line(line):
    if re.match(pattern_variable, line):
        action = 'AFFECT'
        inputs, _, output = line.split()
        inputs = [inputs]
    elif re.match(pattern_opposite, line):
        action = 'NOT'
        _, inputs, _, output = line.split()
        inputs = [inputs]
    elif re.match(pattern_operator, line):
        input_1, action, input_2, _, output = line.split()
        inputs = [input_1, input_2]
    return action, inputs, output

def do_action(gate, inputs, output, wires):
    inputs = [wires[i] if i in wires else int(i) for i in inputs]
    wires[output] = OPERATORS[gate](inputs)
    return wires

def doable(inputs, wires):
    return all([i in wires or i.isdigit() for i in inputs])

def solve_part1(ipt):
    wires = {}
    instructions = ipt.split('\n')
    while len(instructions) > 0:
        gate, inputs, output = parse_line(instructions[0])
        if doable(inputs, wires):
            wires = do_action(gate, inputs, output, wires)
            instructions.pop(0)
        else:
            instructions.append(instructions.pop(0))
    return wires

def solve_part2(ipt, result_part1):
    wires = {}
    instructions = ipt.split('\n')
    while len(instructions) > 0:
        gate, inputs, output = parse_line(instructions[0])
        if doable(inputs, wires):
            if gate == 'AFFECT' and output == 'b':
                inputs[0] = result_part1
            wires = do_action(gate, inputs, output, wires)
            instructions.pop(0)
        else:
            instructions.append(instructions.pop(0))
    return wires


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    final_wires = solve_part1(ipt)
    result = final_wires['a']
    print(result)
    final_wires = solve_part2(ipt, result)
    print(final_wires['a'])