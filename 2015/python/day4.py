#!/usr/bin/env python3

from utils import test

import hashlib

examples_part1 = {
    'abcdef': 609043,
    'pqrstuv': 1048970,
}

examples_part2 = {
}

INPUT = 'resources/day4.txt'

def solve_part1(ipt):
    counter = 0
    while True:
        key = ipt + str(counter)
        current_hash = hashlib.md5(key.encode()).hexdigest()
        if current_hash[:5] == '00000':
            break
        counter += 1
    return counter

def solve_part2(ipt):
    counter = 0
    while True:
        key = ipt + str(counter)
        current_hash = hashlib.md5(key.encode()).hexdigest()
        if current_hash[:6] == '000000':
            break
        counter += 1
    return counter


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    final_counter = solve_part1(ipt)
    print(final_counter)
    final_counter = solve_part2(ipt)
    print(final_counter)