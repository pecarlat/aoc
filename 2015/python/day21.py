#!/usr/bin/env python3

from utils import test

import itertools
from collections import defaultdict

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day21.txt'
WEAPONS = [
    {'cost': 8,  'damage': 4, 'armor': 0},
    {'cost': 10, 'damage': 5, 'armor': 0},
    {'cost': 25, 'damage': 6, 'armor': 0},
    {'cost': 40, 'damage': 7, 'armor': 0},
    {'cost': 74, 'damage': 8, 'armor': 0},
]
ARMORS = [
    {'cost': 0,   'damage': 0, 'armor': 0},
    {'cost': 13,  'damage': 0, 'armor': 1},
    {'cost': 31,  'damage': 0, 'armor': 2},
    {'cost': 53,  'damage': 0, 'armor': 3},
    {'cost': 75,  'damage': 0, 'armor': 4},
    {'cost': 102, 'damage': 0, 'armor': 5},
]
RINGS = [
    {'cost': 0,   'damage': 0, 'armor': 0},
    {'cost': 0,   'damage': 0, 'armor': 0},
    {'cost': 25,  'damage': 1, 'armor': 0},
    {'cost': 50,  'damage': 2, 'armor': 0},
    {'cost': 100, 'damage': 3, 'armor': 0},
    {'cost': 20,  'damage': 0, 'armor': 1},
    {'cost': 40,  'damage': 0, 'armor': 2},
    {'cost': 80,  'damage': 0, 'armor': 3},
]
HP = 100


def get_boss_stats(txt):
    boss_stats = [int(line.split(': ')[1]) for line in txt.split('\n')]
    return {
        'hp':     boss_stats[0],
        'damage': boss_stats[1],
        'armor':  boss_stats[2],
    }

def get_all_possible_combinations():
    weapons = itertools.combinations(WEAPONS, 1)
    armors = itertools.combinations(ARMORS, 1)
    rings = itertools.combinations(RINGS, 2)
    all_combinations = itertools.product(weapons, armors, rings)
    compacted = []
    for combination in all_combinations:
        stuff = defaultdict(lambda: 0)
        for sub in [x for y in combination for x in y]:
            stuff['cost']   += sub['cost']
            stuff['damage'] += sub['damage']
            stuff['armor']  += sub['armor']
        compacted.append(dict(stuff))
    return compacted

def attack(attacker, defender):
    defender['hp'] -= max(1, attacker['damage'] - defender['armor'])
    return defender

def simulate_game(boss_stats, player_stats):
    player_stats['hp'] = HP
    while True:
        boss_stats = attack(player_stats, boss_stats)
        if boss_stats['hp'] <= 0: return True
        player_stats = attack(boss_stats, player_stats)
        if player_stats['hp'] <= 0: return False

def solve_part1(ipt):
    boss = get_boss_stats(ipt)
    combinations = get_all_possible_combinations()
    combinations = sorted(combinations, key=lambda x: x['cost'])
    for combination in combinations:
        won = simulate_game(boss.copy(), combination)
        if won:
            return combination['cost']
    return 0

def solve_part2(ipt):
    boss = get_boss_stats(ipt)
    combinations = get_all_possible_combinations()
    combinations = sorted(combinations, key=lambda x: x['cost'], reverse=True)
    for combination in combinations:
        won = simulate_game(boss.copy(), combination)
        if not won:
            return combination['cost']
    return 0


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    amount_gold = solve_part1(ipt)
    print(amount_gold)
    amount_gold = solve_part2(ipt)
    print(amount_gold)
