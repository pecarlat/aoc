#!/usr/bin/env python3

from utils import test

import re
import json

examples_part1 = {
    '[1,2,3]': 6,
    '{"a":2,"b":4}': 6,
    '[[[3]]]': 3,
    '{"a":{"b":4},"c":-1}': 3,
    '{"a":[-1,1]}': 0,
    '[-1,{"a":1}]': 0,
    '[]': 0,
    '{}': 0,
}

examples_part2 = {
    '[1,2,3]': 6,
    '[1,{"c":"red","b":2},3]': 4,
    '{"d":"red","e":[1,2,3,4],"f":5}': 0,
    '[1,"red",5]': 6,
}

INPUT = 'resources/day12.txt'

def recurse_nb_finder(element):
    if type(element) == int:
        return element
    elif type(element) == list:
        return sum([recurse_nb_finder(e) for e in element])
    elif type(element) == dict and 'red' not in element.values():
        return sum([recurse_nb_finder(e) for e in element.values()])
    else:
        return 0

def solve_part1(ipt):
    return sum([int(e) for e in re.findall("[+-]?\d+(?:\.\d+)?", ipt)])

def solve_part2(ipt):
    return recurse_nb_finder(json.loads(ipt))


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_numbers = solve_part1(ipt)
    print(nb_numbers)
    nb_numbers = solve_part2(ipt)
    print(nb_numbers)