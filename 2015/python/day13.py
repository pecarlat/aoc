#!/usr/bin/env python3

from utils import test

from collections import defaultdict
from itertools import permutations

examples_part1 = {
    ('Alice would gain 54 happiness units by sitting next to Bob.\n'
     'Alice would lose 79 happiness units by sitting next to Carol.\n'
     'Alice would lose 2 happiness units by sitting next to David.\n'
     'Bob would gain 83 happiness units by sitting next to Alice.\n'
     'Bob would lose 7 happiness units by sitting next to Carol.\n'
     'Bob would lose 63 happiness units by sitting next to David.\n'
     'Carol would lose 62 happiness units by sitting next to Alice.\n'
     'Carol would gain 60 happiness units by sitting next to Bob.\n'
     'Carol would gain 55 happiness units by sitting next to David.\n'
     'David would gain 46 happiness units by sitting next to Alice.\n'
     'David would lose 7 happiness units by sitting next to Bob.\n'
     'David would gain 41 happiness units by sitting next to Carol.'): 330,
}

examples_part2 = {
}

INPUT = 'resources/day13.txt'

def lose_or_gain(s):
    return 1 if s == 'gain' else -1

def parse_input(ipt):
    guests = defaultdict(dict)
    for line in ipt.split('\n'):
        guest1, _, action, count, _, _, _, _, _, _, guest2 = line[:-1].split()
        count = int(count)
        guests[guest1][guest2] = count * lose_or_gain(action)
    return guests

def compute_happiness(permutation, guests):
    counter = 0
    for i in range(len(permutation) - 1):
        counter += guests[permutation[i]][permutation[i+1]]
        counter += guests[permutation[i+1]][permutation[i]]
    counter += guests[permutation[-1]][permutation[0]]
    counter += guests[permutation[0]][permutation[-1]]

    return counter

def add_ambivalent(guests, name):
    for guest in guests.keys():
        guests[guest][name] = 0
    guests[name] = {guest: 0 for guest in guests.keys()}
    return guests

def solve_part1(ipt):
    guests = parse_input(ipt)
    maximal_happiness = 0
    all_permutations = permutations(guests)
    any_guest = list(guests.keys())[0]
    all_permutations = [x for x in all_permutations if x[0] == any_guest]
    for permutation in all_permutations:
        happiness = compute_happiness(permutation, guests)
        maximal_happiness = max(happiness, maximal_happiness)

    return maximal_happiness

def solve_part2(ipt):
    guests = parse_input(ipt)
    guests = add_ambivalent(guests, 'I')
    maximal_happiness = 0
    all_permutations = permutations(guests)
    any_guest = list(guests.keys())[0]
    all_permutations = [x for x in all_permutations if x[0] == any_guest]
    for permutation in all_permutations:
        happiness = compute_happiness(permutation, guests)
        maximal_happiness = max(happiness, maximal_happiness)

    return maximal_happiness


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    happiness = solve_part1(ipt)
    print(happiness)
    happiness = solve_part2(ipt)
    print(happiness)