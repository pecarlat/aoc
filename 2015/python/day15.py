#!/usr/bin/env python3

from utils import test

import itertools
import time
from functools import reduce
import re
import random

examples_part1 = {
    ('Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8\n'
     'Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3'): 62842880,
}

examples_part2 = {
    ('Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8\n'
     'Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3'): 57600000,
}

INPUT = 'resources/day15.txt'
NB_UNITS = 100
NB_EPOCHS = 10000

def get_score(ings, props, nb_c):
    w_ings = [[e * props[i] for e in ing] for i, ing in enumerate(ings)]
    summed = [max(0, sum([ing[c] for ing in w_ings])) for c in range(nb_c)]
    return reduce(lambda x, y: x * y, summed)

def get_kcal(ings, props):
    return sum([ing[-1] * props[i] for i, ing in enumerate(ings)])

def get_all_combinations_itertools(n, m):
    numbers = range(m + 1)
    return [c for c in itertools.product(numbers, repeat=n) if sum(c) == m]

def brute_force_search(ingredients, kcal=None):
    max_score = 0
    nb_c = len(ingredients[0]) + (-1 if kcal else 0)
    all_props = get_all_combinations_itertools(len(ingredients), NB_UNITS)
    for combination in all_props:
        max_score = max(max_score, get_score(ingredients, combination, nb_c))

    return max_score

def get_random_proportion(n):
    max_nb = 100 + (n - 1)
    x, p = [], []
    while len(x) < n-1:
        v = random.randint(1, max_nb)
        if not v in x:
            x.append(v)
    x.sort()
    p.append(x[0] - 1)
    for i in range(1, n - 1):
        p.append(x[i] - x[i-1] - 1)
    p.append(max_nb - x[-1])
    return p

def can_update(proportion, p):
    return proportion[p[0]] != NB_UNITS and proportion[p[1]] != 0

def update_proportion(prop, p):
    new_prop = prop
    new_prop[p[0]] += 1
    new_prop[p[1]] -= 1
    return new_prop

def local_maximas(ingredients, kcal=None):
    max_score = 0
    max_score_with_kcal = 0
    nb_c = len(ingredients[0]) + (-1 if kcal else 0)
    for i in range(NB_EPOCHS):
        proportion = get_random_proportion(len(ingredients))
        score = get_score(ingredients, proportion, nb_c)
        permutations = itertools.permutations(range(len(ingredients)), 2)
        calories = get_kcal(ingredients, proportion)
        score_with_kcal = (score if calories == kcal else 0)

        has_changed = True
        nb_steps = 0
        while has_changed:
            has_changed = False
            for permutation in permutations:
                while True:
                    if not can_update(proportion, permutation):
                        break
                    new_proportion = update_proportion(proportion, permutation)
                    new_score = get_score(ingredients, new_proportion, nb_c)
                    calories = get_kcal(ingredients, proportion)
                    if new_score >= score:
                        has_changed = True
                        nb_steps += 1
                        proportion = new_proportion
                        score = new_score
                        score_with_kcal = (score if calories == kcal else score_with_kcal)
                    else:
                        break

        max_score = max(max_score, score)
        max_score_with_kcal = max(max_score_with_kcal, score_with_kcal)
        if i % 50 == 0:
            print('Epoch', i, '->', max_score_with_kcal if kcal else max_score)

    return max_score_with_kcal if kcal else max_score

def solve_part1(ipt):
    ingredients = []
    regex = r'(\w+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)'
    for _, c, d, f, t, _ in re.findall(regex, ipt):
        ingredients.append([int(e) for e in [c, d, f, t]])

    start = time.time()
    solution_bfs = brute_force_search(ingredients)
    print('BFS done in', time.time() - start)

    start = time.time()
    solution_lm = local_maximas(ingredients)
    print('Local Maximas done in', time.time() - start)

    assert solution_bfs == solution_lm, \
        'Found a local maxima, should increase the epochs numbers'

    return solution_lm

def solve_part2(ipt):
    ingredients = []
    regex = r'(\w+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)'
    for _, c, d, f, t, kcal in re.findall(regex, ipt):
        ingredients.append([int(e) for e in [c, d, f, t, kcal]])

    start = time.time()
    solution_lm = local_maximas(ingredients, kcal=500)
    print('Local Maximas done in', time.time() - start)

    return solution_lm


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    highest_score = solve_part1(ipt)
    print(highest_score)
    highest_score = solve_part2(ipt)
    print(highest_score)