#!/usr/bin/env python3

from utils import test

examples_part1 = {
    '110': 6,
    '130': 8,
    '150': 8,
}

examples_part2 = {
}

INPUT = 'resources/day20.txt'


def solve_part1(ipt):
    nb_presents = int(ipt)
    houses = [10] * (nb_presents // 10)
    for i in range(2, nb_presents // 10):
        for j in range(i, nb_presents // 10, i):
            houses[j] += i * 10

    return [j for j, house in enumerate(houses) if house >= nb_presents][0]

def solve_part2(ipt):
    nb_presents = int(ipt)
    houses = [11] * (nb_presents // 11)
    for i in range(2, nb_presents // 11):
        for j in range(50):
            if i + i * j < nb_presents // 11:
                houses[i + i * j] += i * 11

    return [j for j, house in enumerate(houses) if house >= nb_presents][0]


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    house_number = solve_part1(ipt)
    print(house_number)
    house_number = solve_part2(ipt)
    print(house_number)
