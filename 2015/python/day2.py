#!/usr/bin/env python3

from utils import test

import itertools
from functools import reduce
from operator import mul

examples_part1 = {
    '2x3x4': 58,
    '1x1x10': 43,
}

examples_part2 = {
    '2x3x4': 34,
    '1x1x10': 14,
}

INPUT = 'resources/day2.txt'


def solve_part1(ipt):
    total_wrapping = 0
    for line in ipt.split('\n'):
        dimensions = [int(d) for d in line.split('x')]
        combinations = itertools.combinations(dimensions, 2)
        sq_feets = [c[0] * c[1] for c in combinations]
        total_wrapping += (2 * sum(sq_feets)) + min(sq_feets)
    return total_wrapping

def solve_part2(ipt):
    total_ribbon = 0
    for line in ipt.split('\n'):
        dimensions = [int(d) for d in line.split('x')]
        bow = reduce(mul, dimensions, 1)
        dimensions.remove(max(dimensions))
        ribbon = 2 * sum(dimensions)
        total_ribbon += ribbon + bow
    return total_ribbon


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    wrapping_paper_surface = solve_part1(ipt)
    print(wrapping_paper_surface)
    feet_of_ribbon = solve_part2(ipt)
    print(feet_of_ribbon)