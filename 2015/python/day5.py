#!/usr/bin/env python3

from utils import test

examples_part1 = {
    'ugknbfddgicrmopn': 1,
    'aaa': 1,
    'jchzalrnumimnmhp': 0,
    'haegwjzuvuyypxyu': 0,
    'dvszwmarrgswjxmb': 0,
}

examples_part2 = {
    'qjhvhtzxzqqjkmpb': 1,
    'xxyxx': 1,
    'uurcxstgmygtbstg': 0,
    'ieodomkazucvgmuy': 0,
}

INPUT = 'resources/day5.txt'
VOYELS = ['a', 'e', 'i', 'o', 'u']

def solve_part1(ipt):
    naughty = ['ab', 'cd', 'pq', 'xy']
    nb_nice_strings = 0
    for line in ipt.split('\n'):
        nb_voyels = 0
        has_naughty = False
        has_double = False
        for i in range(len(line) - 1):
            nb_voyels += int(line[i] in VOYELS)
            if line[i:i+2] in naughty:
                has_naughty = True
            if line[i] == line[i+1]:
                has_double = True
        nb_voyels += int(line[-1] in VOYELS)

        is_nice = nb_voyels >= 3 and has_double and not has_naughty
        nb_nice_strings += int(is_nice)

    return nb_nice_strings

def solve_part2(ipt):
    nb_nice_strings = 0
    for line in ipt.split('\n'):
        all_pairs = [line[:2]]
        has_in_between = False
        for i in range(1, len(line) - 1):
            if all_pairs[-1] != line[i:i+2] or all_pairs[-1] == line[i-2:i]:
                all_pairs.append(line[i:i+2])
            if line[i-1] == line[i+1]:
                has_in_between = True

        has_double_pairs = len(all_pairs) != len(set(all_pairs))

        is_nice = has_double_pairs and has_in_between
        nb_nice_strings += int(is_nice)

    return nb_nice_strings


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_nice_strings = solve_part1(ipt)
    print(nb_nice_strings)
    nb_nice_strings = solve_part2(ipt)
    print(nb_nice_strings)