#!/usr/bin/env python3

from utils import test

import random
import re

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day19.txt'
LIMIT_PER_UPDATE = 100


def replace(old, new, index, seq):
    return seq[:index] + new + seq[index + len(old):]

def get_all_variations(seq, replacements):
    molecules = []
    for chain, replac in replacements:
        regex = '(?={})'.format(chain)
        indexes = [m.start(0) for m in re.finditer(regex, seq)]
        molecules += [replace(chain, replac, i, seq) for i in indexes]
    return molecules

def solve_part1(ipt):
    data = ipt.split('\n')
    replacements = [line.split(' => ') for line in data[:-2]]
    return len(set(get_all_variations(data[-1], replacements)))

def solve_part2(ipt):
    data = ipt.split('\n')
    replacements = [tuple(line.split(' => ')) for line in data[:-2]]
    sequence = data[-1]
    nb_iters = 0

    while sequence != 'e':
        flag = False
        for a, b in replacements:
            if b not in sequence:
                continue

            sequence = sequence.replace(b, a, 1)
            nb_iters += 1
            flag = True

        if not flag:
            sequence = data[-1]
            nb_iters = 0
            random.shuffle(replacements)

    return nb_iters


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_molecules = solve_part1(ipt)
    print(nb_molecules)
    nb_molecules = solve_part2(ipt)
    print(nb_molecules)
