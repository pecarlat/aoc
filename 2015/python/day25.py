#!/usr/bin/env python3

from utils import test

examples_part1 = {
    'To continue, please consult the code grid in the manual.  Enter the code at row 2, column 1.': 31916031,
}

examples_part2 = {
}

INPUT = 'resources/day25.txt'
FIRST = 20151125

def next_code(code):
    return (code * 252533) % 33554393

def solve_part1(ipt):
    row = int(ipt.split()[-3].replace(',', ''))
    column = int(ipt.split()[-1].replace('.', ''))
    code_nb = sum(range(row + column)) - row
    code = FIRST
    for i in range(code_nb):
        code = next_code(code)
    return code

def solve_part2(ipt):
    return 0


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    code = solve_part1(ipt)
    print(code)
    code = solve_part2(ipt)
    print(code)
