#!/usr/bin/env python3

from utils import test

import sys
from itertools import permutations

examples_part1 = {
    ('London to Dublin = 464\n'
     'London to Belfast = 518\n'
     'Dublin to Belfast = 141'): 605,
}

examples_part2 = {
    ('London to Dublin = 464\n'
     'London to Belfast = 518\n'
     'Dublin to Belfast = 141'): 982,
}

INPUT = 'resources/day9.txt'

def compute_distance(graph, p):
    return sum([graph[p[i]][p[i+1]] for i in range(len(p) - 1)])

def build_graph(txt):
    graph = {}
    for connection in txt.split('\n'):
        _from, _, _to, _, _distance = connection.split()
        graph.setdefault(_from, {})
        graph.setdefault(_to, {})
        graph[_from][_to] = int(_distance)
        graph[_to][_from] = int(_distance)
    return graph

def solve_part1(ipt):
    graph = build_graph(ipt)
    
    shortest = sys.maxsize
    for permutation in permutations(graph.keys()):
        distance = compute_distance(graph, permutation)
        if distance < shortest:
            shortest = distance

    return shortest

def solve_part2(ipt):
    graph = build_graph(ipt)
    
    longest = 0
    for permutation in permutations(graph.keys()):
        distance = compute_distance(graph, permutation)
        if distance > longest:
            longest = distance

    return longest


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    shortest_distance = solve_part1(ipt)
    print(shortest_distance)
    longest_distance = solve_part2(ipt)
    print(longest_distance)