#!/usr/bin/env python3

from utils import test

from collections import defaultdict

examples_part1 = {
    ('inc b\n'
     'jio b, +2\n'
     'tpl b\n'
     'inc b'): 2,
}

examples_part2 = {
}

INPUT = 'resources/day23.txt'

def solve_instructions(registers, instructions):
    current_idx = 0
    while 0 <= current_idx < len(instructions):
        instruction = instructions[current_idx]
        if instruction[0] == 'hlf':
            registers[instruction[1]] /= 2
        elif instruction[0] == 'tpl':
            registers[instruction[1]] *= 3
        elif instruction[0] == 'inc':
            registers[instruction[1]] += 1
        elif instruction[0] == 'jmp':
            current_idx += int(instruction[1]) - 1
        elif instruction[0] == 'jie' and registers[instruction[1]] % 2 == 0:
            current_idx += int(instruction[2]) - 1
        elif instruction[0] == 'jio' and registers[instruction[1]] == 1:
            current_idx += int(instruction[2]) - 1
        current_idx += 1
    return registers

def solve_part1(ipt):
    ipt = ipt.replace(',', '')
    instructions = [line.split() for line in ipt.split('\n')]
    registers = solve_instructions(defaultdict(lambda: 0), instructions)
    return registers['b']

def solve_part2(ipt):
    ipt = ipt.replace(',', '')
    instructions = [line.split() for line in ipt.split('\n')]
    registers = defaultdict(lambda: 0)
    registers['a'] = 1
    registers = solve_instructions(registers, instructions)
    return registers['b']


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    register = solve_part1(ipt)
    print(register)
    register = solve_part2(ipt)
    print(register)
