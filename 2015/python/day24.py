#!/usr/bin/env python3

from utils import test

from functools import reduce
from operator import mul
import itertools

examples_part1 = {
    '1\n2\n3\n4\n5\n7\n8\n9\n10\n11': 99,
}

examples_part2 = {
    '1\n2\n3\n4\n5\n7\n8\n9\n10\n11': 44,
}

INPUT = 'resources/day24.txt'

def can_divide(weights, weight_unit, nb_groups):
    if nb_groups == 1:
        return True

    there_is_hope = False
    for i in range(1, len(weights) - (nb_groups - 1)):
        all_group1 = itertools.combinations(weights, i)
        for group1 in [g for g in all_group1 if sum(g) == weight_unit]:
            other_groups = [w for w in weights if w not in group1]
            if can_divide(other_groups, weight_unit, nb_groups - 1):
                return True
    return False


def find_groups(weights, nb_groups):
    possible_configurations = []
    for i in range(1, len(weights) - 1):
        all_group1 = itertools.combinations(weights, i)
        for group1 in all_group1:
            other_groups = [w for w in weights if w not in group1]
            if sum(other_groups) == sum(group1) * (nb_groups - 1):
                if can_divide(other_groups, sum(group1), nb_groups - 1):
                    possible_configurations.append(group1)
        if len(possible_configurations) > 0:
            break

    return possible_configurations

def solve_part1(ipt):
    weights = [int(line) for line in ipt.split('\n')]
    possible_groups_1 = find_groups(weights, 3)
    return min([reduce(mul, group) for group in possible_groups_1])

def solve_part2(ipt):
    weights = [int(line) for line in ipt.split('\n')]
    possible_groups_1 = find_groups(weights, 4)
    return min([reduce(mul, group) for group in possible_groups_1])


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    qe = solve_part1(ipt)
    print(qe)
    qe = solve_part2(ipt)
    print(qe)
