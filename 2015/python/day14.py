#!/usr/bin/env python3

from utils import test

import re
import itertools

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day14.txt'
CHECK_TIME = 2503


def solve_part1(ipt):
    regex = r'(\w+) can fly (\d+) km/s for (\d+) seconds, but then must ' + \
             'rest for (\d+) seconds.'
    reindeers = []
    for _, speed, moving_time, rest_time in re.findall(regex, ipt):
        speed = int(speed)
        moving_time, rest_time = int(moving_time), int(rest_time)
        nb, rem = divmod(CHECK_TIME, (moving_time + rest_time))
        total_moving_time = (nb * moving_time + min(rem, moving_time))
        distance = total_moving_time * speed
        reindeers.append(distance)

    return max(reindeers)

def solve_part2(ipt):
    regex = r'(\w+) can fly (\d+) km/s for (\d+) seconds, but then must ' + \
             'rest for (\d+) seconds.'
    reindeers = []
    for _, speed, moving_time, rest_time in re.findall(regex, ipt):
        speed = int(speed)
        moving_time, rest_time = int(moving_time), int(rest_time)
        plan = itertools.cycle([speed] * moving_time + [0] * rest_time)
        acc_plan = itertools.accumulate(next(plan) for _ in range(CHECK_TIME))
        reindeers.append(list(acc_plan))

    scores = [0] * len(reindeers)
    for step in zip(*reindeers):
        scores = [scores[i] + 1 if v == max(step) else scores[i]
                  for i, v in enumerate(step)]

    return max(scores)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    distance = solve_part1(ipt)
    print(distance)
    distance = solve_part2(ipt)
    print(distance)