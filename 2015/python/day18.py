#!/usr/bin/env python3

from utils import test

import numpy as np

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day18.txt'
NB_STEPS = 100


def do_step(a, corners=False):
    new_a = np.zeros(a.shape)
    for i in range(1, 101):
        for j in range(1, 101):
            sub_arr = a[i-1:i+2,j-1:j+2]
            if (a[i,j] and np.sum(sub_arr) in [3, 4]) or \
               (not a[i,j] and np.sum(sub_arr) == 3):
                new_a[i,j] = 1
    if corner:
        new_a[[1, 1, 100, 100], [1, 100, 1, 100]] = 1
    return new_a

def solve_part1(ipt):
    lights = [[(1 if light == '#' else 0) for light in line]
              for line in ipt.split('\n')]
    array = np.zeros((102, 102))
    array[1:-1,1:-1] = np.array(lights)

    for _ in range(NB_STEPS):
        array = do_step(array)

    return np.sum(array)

def solve_part2(ipt):
    lights = [[(1 if light == '#' else 0) for light in line]
              for line in ipt.split('\n')]
    array = np.zeros((102, 102))
    array[1:-1,1:-1] = np.array(lights)
    array[[1, 1, 100, 100], [1, 100, 1, 100]] = 1

    for _ in range(NB_STEPS):
        array = do_step(array, corners=True)

    return np.sum(array)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_lights = solve_part1(ipt)
    print(nb_lights)
    nb_lights = solve_part2(ipt)
    print(nb_lights)