#!/usr/bin/env python3

from utils import test

from collections import namedtuple
from heapq import heappop, heappush

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day22.txt'
HP_START = 50
MANA_START = 500


class Spell(namedtuple('Spell',
                       'name cost effect turns damage heal armor mana')):
    def __new__(cls, name, cost, effect=False, turns=None, damage=0, heal=0,
                armor=0, mana=0):
        return super().__new__(
            cls, name, cost, effect, turns, damage, heal, armor, mana)

    def is_castable(self, mana, effects):
        return self.cost <= mana and all(self is not s for _, s in effects)

    def cast_spell(self, state):
        if not self.effect:
            state.player = (state.player[0] + self.heal, state.player[1])
            state.boss = (state.boss[0] - self.damage, state.boss[1])
        else:
            state.effects += ((self.turns, self),)
        return state

spells = (
    Spell('Magic Missile', 53,  damage=4),
    Spell('Drain',         73,  damage=2, heal=2),
    Spell('Shield',        113, effect=True, turns=6, armor=7),
    Spell('Poison',        173, effect=True, turns=6, damage=3),
    Spell('Recharge',      229, effect=True, turns=5, mana=101),
)

class State(object):
    def __init__(self, player, boss, mana_spent=0, effects=None):
        self.player = player
        self.boss = boss
        self.mana_spent = mana_spent
        self.effects = effects or ()

    def __eq__(self, other):
        if not isinstance(other, State):
            return NotImplemented
        return all(getattr(self, k) == getattr(other, k) for k in vars(self))

    def process_effects(self):
        remaining_effects = []
        armor = 0
        hp, mana = self.player
        boss_hp, boss_dmg = self.boss
        for timer, effect in self.effects:
            hp += effect.heal
            mana += effect.mana
            boss_hp -= effect.damage
            armor = max(armor, effect.armor)
            if timer > 1:
                remaining_effects.append((timer - 1, effect))
        return tuple(remaining_effects), (hp, mana), (boss_hp, boss_dmg), armor

    def boss_turn(self):
        self.effects, self.player, self.boss, armor = self.process_effects()
        # only if the boss is still alive can they attack!
        if self.boss[0] > 0:
            new_hp = self.player[0] - max(1, self.boss[1] - armor)
            self.player = (new_hp, self.player[1])

    def transitions(self, part2=False):
        # Player turn first
        if part2:
            self.player = (self.player[0] - 1, self.player[1])
        effects, player, boss, _ = self.process_effects()

        new_states = []
        for spell in spells:
            # Unavailable spells
            if not spell.is_castable(player[1], effects):
                continue

            # Next state, using the current spell
            new_state = State((player[0], player[1] - spell.cost),
                              boss,
                              self.mana_spent + spell.cost,
                              effects)
            new_state = spell.cast_spell(new_state)

            # Boss turn next
            new_state.boss_turn()

            # No point in playing a turn that has the player losing
            if new_state.player[0] > 0:
                new_states.append(new_state)

        return new_states

def djikstra_search(start, part2=False):
    open_states = [start]
    pqueue = [(0, 0, start)]
    tuple_id = 1
    while open_states:
        current = heappop(pqueue)[-1]
        if current.boss[0] < 1:
            return current
        open_states.remove(current)
        for state in current.transitions(part2):
            if state in open_states:
                continue
            open_states.append(state)
            heappush(pqueue, (state.mana_spent, tuple_id, state))
            tuple_id += 1

def solve_part1(ipt):
    player = (HP_START, MANA_START)
    boss = tuple([int(line.split(': ')[1]) for line in ipt.split('\n')])
    start = State(player, boss)
    return djikstra_search(start).mana_spent

def solve_part2(ipt):
    player = (HP_START, MANA_START)
    boss = tuple([int(line.split(': ')[1]) for line in ipt.split('\n')])
    start = State(player, boss)
    return djikstra_search(start, part2=True).mana_spent


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    least_mana = solve_part1(ipt)
    print(least_mana)
    least_mana = solve_part2(ipt)
    print(least_mana)
