#!/usr/bin/env python3

from utils import test

examples_part1 = {
    r'""': 2,
    r'"abc"': 2,
    r'"aaa\"aaa"': 3,
    r'"\x27"': 5,
}

examples_part2 = {
    r'""': 4,
    r'"abc"': 4,
    r'"aaa\"aaa"': 6,
    r'"\x27"': 5,
}

INPUT = 'resources/day8.txt'


def solve_part1(ipt):
    return sum([len(line) - len(eval(line)) for line in ipt.split('\n')])

def solve_part2(ipt):
    extended_length = lambda s: 2 + len(s) + s.count('"') + s.count('\\')
    return sum([extended_length(line) - len(line) for line in ipt.split('\n')])


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    final_nb_char = solve_part1(ipt)
    print(final_nb_char)
    final_nb_char = solve_part2(ipt)
    print(final_nb_char)