#!/usr/bin/env python3

from utils import test

from collections import defaultdict

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day25.txt'


def solve_part1(ipt):
    # cpy x c and cpy y b on lines 2 and 3
    target = 15 * 170
    n = 1
    while n < target:
        if n % 2 == 0:
            n = n * 2 + 1
        else:
            n *= 2
    return n - target

def solve_part2(ipt):
    return 0


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    smallest_int = solve_part1(ipt)
    print(smallest_int)
    smallest_int = solve_part2(ipt)
    print(smallest_int)