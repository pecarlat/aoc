#!/usr/bin/env python3

from utils import test

examples_part1 = {
    ('ULL\n'
     'RRDDD\n'
     'LURDL\n'
     'UUUUD'): '1985',
}

examples_part2 = {
    ('ULL\n'
     'RRDDD\n'
     'LURDL\n'
     'UUUUD'): '5DB3',
}

INPUT = 'resources/day2.txt'
KEYPAD = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
]
KEYPAD_2 = [
    [None,  None, 1,   None, None],
    [None,  2,    3,   4,    None],
    [5,     6,    7,   8,    9],
    [None, 'A',   'B', 'C',  None],
    [None,  None, 'D', None, None],
]


def is_in_keypad(pos, keypad):
    return keypad[pos[0]][pos[1]] != None

def cast_value(min_v, max_v, v):
    return min(max(v, min_v), max_v)

def move(position, direction, keypad):
    sbs = {
        'U': (-1, 0),
        'R': (0, 1),
        'D': (1, 0),
        'L': (0, -1),
    }[direction]
    x = cast_value(0, len(keypad) - 1,    position[0] + sbs[0])
    y = cast_value(0, len(keypad[0]) - 1, position[1] + sbs[1])
    if not is_in_keypad((x, y), keypad):
        x, y = position
    return (x, y)

def solve_part1(ipt):
    instructions = [instruction for instruction in ipt.split()]
    position = (1, 1)
    code = []
    for instruction in instructions:
        for direction in instruction:
            position = move(position, direction, KEYPAD)
        code.append(str(KEYPAD[position[0]][position[1]]))
    return ''.join(code)

def solve_part2(ipt):
    instructions = [instruction for instruction in ipt.split()]
    position = (2, 0)
    code = []
    for instruction in instructions:
        for direction in instruction:
            position = move(position, direction, KEYPAD_2)
        code.append(str(KEYPAD_2[position[0]][position[1]]))
    return ''.join(code)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    code = solve_part1(ipt)
    print(code)
    code = solve_part2(ipt)
    print(code)