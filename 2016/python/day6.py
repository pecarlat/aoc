#!/usr/bin/env python3

from utils import test

import hashlib

examples_part1 = {
    ('eedadn\n'
     'drvtee\n'
     'eandsr\n'
     'raavrd\n'
     'atevrs\n'
     'tsrnev\n'
     'sdttsa\n'
     'rasrtv\n'
     'nssdts\n'
     'ntnada\n'
     'svetve\n'
     'tesnvt\n'
     'vntsnd\n'
     'vrdear\n'
     'dvrsen\n'
     'enarar'): 'easter',
}

examples_part2 = {
    ('eedadn\n'
     'drvtee\n'
     'eandsr\n'
     'raavrd\n'
     'atevrs\n'
     'tsrnev\n'
     'sdttsa\n'
     'rasrtv\n'
     'nssdts\n'
     'ntnada\n'
     'svetve\n'
     'tesnvt\n'
     'vntsnd\n'
     'vrdear\n'
     'dvrsen\n'
     'enarar'): 'advent',
}

INPUT = 'resources/day6.txt'


def solve_part1(ipt):
    all_lines = ipt.split()
    columns = [[] for i in range(len(all_lines[0]))]
    for line in all_lines:
        for i, char in enumerate(line):
            columns[i].append(char)
    return ''.join([max(col, key=col.count) for col in columns])

def solve_part2(ipt):
    all_lines = ipt.split()
    columns = [[] for i in range(len(all_lines[0]))]
    for line in all_lines:
        for i, char in enumerate(line):
            columns[i].append(char)
    return ''.join([min(col, key=col.count) for col in columns])


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    message = solve_part1(ipt)
    print(message)
    message = solve_part2(ipt)
    print(message)