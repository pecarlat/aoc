#!/usr/bin/env python3

from utils import test

import re

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day22.txt'


def parse_input(ipt):
    nodes = {}
    for line in ipt.splitlines()[2:]:
        x, y, size, used, avail, _ = map(int, re.findall(r'\d+', line))
        nodes[(x, y)] = {'used': used, 'avail': avail}
    return nodes

def get_furthest(nodes):
    return max([v[0] for v in nodes.keys()]), max([v[1] for v in nodes.keys()])

def get_empty(nodes):
    for k, v in nodes.items():
        if v['used'] == 0:
            return k
    return -1

def is_viable(v1, v2):
    return v1['used'] != 0 and v1['used'] <= v2['avail']

def solve_part1(ipt):
    nodes = parse_input(ipt)
    count = 0
    vals = list(nodes.values())
    for i in range(len(vals)):
        for j in range(i + 1, len(vals)):
            count += int(is_viable(vals[i], vals[j]))
            count += int(is_viable(vals[j], vals[i]))

    return count

def solve_part2(ipt):
    nodes = parse_input(ipt)
    max_x, max_y = get_furthest(nodes)
    empty_x, empty_y = get_empty(nodes)

    start = (0, 0)
    goal = (max_x - 1, 0)
    empty_size = nodes[(empty_x, empty_y)]['avail']
    maze = [['.' for _ in range(max_y)] for _ in range(max_x)]
    maze[start[0]][start[1]] = 'S'
    maze[goal[0]][goal[1]] = 'G'
    maze[empty_x][empty_y] = 'E'
    for i in range(max_x):
        for j in range(max_y):
            if nodes[(i, j)]['used'] > empty_size:
                maze[i][j] = '#'

    for i in range(max_x):
        line = ''
        for j in range(max_y):
            line += maze[i][j]
        print(line)

    print('Can be counted by hand, move E to S, then to G., # are walls.')

    return 0


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    counter = solve_part1(ipt)
    print(counter)
    solve_part2(ipt)