#!/usr/bin/env python3

from utils import test

from collections import deque
from itertools import permutations

examples_part1 = {
    ('###########\n'
     '#0.1.....2#\n'
     '#.#######.#\n'
     '#4.......3#\n'
     '###########'): 14,
}

examples_part2 = {
}

INPUT = 'resources/day24.txt'


def get_points_of_interest(maze):
    pois = {}
    for i, line in enumerate(maze):
        for j, pos in enumerate(line):
            if pos.isdigit():
                pois[int(pos)] = (i, j)
    return pois

def ok_move_around(maze, pos):
    for move in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
        if maze[pos[0] + move[0]][pos[1] + move[1]] != '#':
            yield (pos[0] + move[0], pos[1] + move[1])

def bfs_from_to(maze, from_pos, to_pos):
    queue = deque([(0, from_pos)])
    been = set([from_pos])
    while queue:
        distance, curr = queue.pop()
        if curr == to_pos:
            return distance
        for move in [m for m in ok_move_around(maze, curr) if m not in been]:
            queue.appendleft((distance + 1, move))
            been.add(move)
    return -1

def get_distance(distances, pos):
    if pos[1] in distances[pos[0]]:
        return distances[pos[0]][pos[1]]
    else:
        return distances[pos[1]][pos[0]]

def compute_distance(distances, p):
    return sum([get_distance(distances, p[i:i+2]) for i in range(len(p) - 1)])

def solve_part1(ipt):
    maze = ipt.splitlines()
    pois = get_points_of_interest(maze)
    max_poi = max(pois.keys())
    distances = {}
    for n in pois.keys():
        distances[n] = {to_n: bfs_from_to(maze, pois[n], pos_n)
                        for to_n, pos_n in pois.items() if to_n > n}
    perms = permutations(range(1, max_poi + 1))
    all_paths = [[0] + list(path) for path in list(perms)]

    shortest_distance = compute_distance(distances, all_paths[0])
    for path in all_paths[1:]:
        distance = compute_distance(distances, path)
        shortest_distance = min(shortest_distance, distance)

    return shortest_distance

def solve_part2(ipt):
    maze = ipt.splitlines()
    pois = get_points_of_interest(maze)
    max_poi = max(pois.keys())
    distances = {}
    for n in pois.keys():
        distances[n] = {to_n: bfs_from_to(maze, pois[n], pos_n)
                        for to_n, pos_n in pois.items() if to_n > n}
    perms = permutations(range(1, max_poi + 1))
    all_paths = [[0] + list(path) + [0] for path in list(perms)]

    shortest_distance = compute_distance(distances, all_paths[0])
    for path in all_paths[1:]:
        distance = compute_distance(distances, path)
        shortest_distance = min(shortest_distance, distance)

    return shortest_distance


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_steps = solve_part1(ipt)
    print(nb_steps)
    nb_steps = solve_part2(ipt)
    print(nb_steps)