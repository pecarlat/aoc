#!/usr/bin/env python3

from utils import test

import hashlib

examples_part1 = {
    'abc': 22728,
}

examples_part2 = {
    'abc': 22551,
}

INPUT = 'resources/day14.txt'


def get_hash(key):
    return hashlib.md5(key.encode()).hexdigest()

def get_inception_hash(key, inception):
    hsh = get_hash(key)
    for i in range(inception):
        hsh = get_hash(hsh)
    return hsh

def find_chars_row(seq, nb):
    for i in range(len(seq) - nb + 1):
        if len(set([seq[j] for j in range(i, i + nb)])) == 1:
            return seq[i]
    return None

def is_valid_hash(hashes):
    triplet = find_chars_row(hashes[0], 3)
    if triplet:
        quintet = ''.join([triplet for _ in range(5)])
        for hsh in hashes[1:]:
            if quintet in hsh:
                return True
    return False

def solve_part1(ipt):
    hashes = [get_hash(ipt + str(i)) for i in range(1000)]

    idx = 0
    valid_pad_keys_nb = 0
    while valid_pad_keys_nb < 64:
        hashes += [get_hash(ipt + str(idx + 1000))]
        if is_valid_hash(hashes):
            valid_pad_keys_nb += 1
        hashes = hashes[1:]
        idx += 1
    return idx - 1

def solve_part2(ipt):
    hashes = [get_inception_hash(ipt + str(i), 2016) for i in range(1000)]

    idx = 0
    valid_pad_keys_nb = 0
    while valid_pad_keys_nb < 64:
        hashes += [get_inception_hash(ipt + str(idx + 1000), 2016)]
        if is_valid_hash(hashes):
            valid_pad_keys_nb += 1
        hashes = hashes[1:]
        idx += 1
    return idx - 1


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    index = solve_part1(ipt)
    print(index)
    index = solve_part2(ipt)
    print(index)