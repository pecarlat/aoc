#!/usr/bin/env python3

from utils import test

import re
from itertools import combinations
import heapq

examples_part1 = {
    ('The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.\n'
     'The second floor contains a hydrogen generator.\n'
     'The third floor contains a lithium generator.\n'
     'The fourth floor contains nothing relevant.'): 11,
}

examples_part2 = {
}

INPUT = 'resources/day11.txt'
PRIORITY_RATIO = 5

class PriorityQueue:
    def __init__(self):
        self.elements = []

    def empty(self):
        return len(self.elements) == 0

    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))

    def get(self):
        return heapq.heappop(self.elements)[1]

def parse_input(ipt):
    mapping = {}
    next_unique_id = 1
    state = []
    for line in ipt.splitlines():
        state.append([])
        for element in re.findall(r'a (\w+-compatible|\w+) (\w+)', line):
            name = element[0].replace('-compatible', '')
            if not name in mapping:
                mapping[name] = next_unique_id
                next_unique_id += 1
            is_generator = 1 if element[1] == 'generator' else -1
            state[-1].append(mapping[name] * is_generator)
    return tuple([tuple(sorted(floor)) for floor in state])

def is_final(state):
    lvl, floors = state
    return (lvl == 3) and all(len(f) == 0 for f in floors[:-1])

def is_valid(elevator, floor):
    tmp_floor = tuple(sorted(floor + elevator))
    if len(tmp_floor) > 0 and tmp_floor[-1] > 0:
        return all((-chip in tmp_floor) for chip in tmp_floor if chip < 0)
    else:
        return True

def compute_next_states(state):
    lvl, floors = state
    next_states = []
    possible_elevators = list(combinations(floors[lvl], 2)) + \
                         list(combinations(floors[lvl], 1))

    for elev in possible_elevators:
        for i in [lvl + 1, lvl - 1]:
            if i < 0 or i >= 4 or not is_valid(elev, floors[i]):
                continue
            tmp = list(floors)
            tmp[lvl] = tuple(sorted([f for f in floors[lvl] if f not in elev]))
            tmp[i] = tuple(sorted(floors[i] + elev))
            new_state = (i, tuple(tmp))
            next_states.append(new_state)

    return next_states

def resolves(costs, current_paths):
    # Early exit flag
    min_path_length = None
    found_final_state = False

    while not current_paths.empty() and not found_final_state:
        current_state = current_paths.get()

        next_states = compute_next_states(current_state)

        for next_state in next_states:
            new_cost = costs[current_state] + 1
            
            # Filtering of non-efficient 'next_state'
            if (next_state not in costs) or new_cost < costs[next_state]:
                if is_final(next_state):
                    found_final_state = True
                    if min_path_length is None:
                        min_path_length = new_cost
                    else:
                        min_path_length = min(min_path_length, new_cost)
                else:
                    costs[next_state] = new_cost
                    i, floors = next_state
                    nb_items_top_floor = len(floors[3])
                    priority = new_cost - nb_items_top_floor * PRIORITY_RATIO
                    current_paths.put(next_state, priority)

    return min_path_length

def solve_part1(ipt):
    state = (0, parse_input(ipt))
    min_path_length = None
    costs = {state: 0}

    # Paths to explore
    current_paths = PriorityQueue()
    current_paths.put(state, 0)

    return resolves(costs, current_paths)

def solve_part2(ipt):
    tmp = parse_input(ipt)
    max_elem = max(x for y in tmp for x in y)
    e1, e2 = max_elem + 1, max_elem + 2
    new_state = [tuple(list(tmp[0]) + [e1, -e1, e2, -e2])] + list(tmp[1:])
    state = (0, tuple(new_state))
    min_path_length = None
    costs = {state: 0}

    # Paths to explore
    current_paths = PriorityQueue()
    current_paths.put(state, 0)

    return resolves(costs, current_paths)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_steps = solve_part1(ipt)
    print(nb_steps)
    nb_steps = solve_part2(ipt)
    print(nb_steps)