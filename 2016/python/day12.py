#!/usr/bin/env python3

from utils import test

from collections import defaultdict

examples_part1 = {
    ('cpy 41 a\n'
     'inc a\n'
     'inc a\n'
     'dec a\n'
     'jnz a 2\n'
     'dec a'): 42,
}

examples_part2 = {
}

INPUT = 'resources/day12.txt'


def unstack_instructions(instructions, registers):
    idx = 0
    while 0 <= idx < len(instructions):
        instruction = instructions[idx]
        action = instruction[0]
        if action in ['cpy', 'jnz']:
            value, register = instruction[1:]
            value = int(value) if value.isdigit() else registers[value]
            if action == 'cpy':
                registers[register] = value
            elif value != 0:
                idx += int(register) - 1
        elif action == 'inc' or action == 'dec':
            register = instruction[1]
            registers[register] += 1 if action == 'inc' else -1
        idx += 1
    return registers

def solve_part1(ipt):
    instructions = [line.split() for line in ipt.splitlines()]
    registers = defaultdict(int)
    registers = unstack_instructions(instructions, registers)
    return registers['a']

def solve_part2(ipt):
    instructions = [line.split() for line in ipt.splitlines()]
    registers = defaultdict(int)
    registers['c'] = 1
    registers = unstack_instructions(instructions, registers)
    return registers['a']


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    register_a = solve_part1(ipt)
    print(register_a)
    register_a = solve_part2(ipt)
    print(register_a)