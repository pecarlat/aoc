#!/usr/bin/env python3

from utils import test

import hashlib

examples_part1 = {
    'abc': '18f47a30',
}

examples_part2 = {
    'abc': '05ace8e3',
}

INPUT = 'resources/day5.txt'


def get_hash(key):
    return hashlib.md5(key.encode()).hexdigest()

def solve_part1(ipt):
    code = []
    key = ipt
    current_idx = 0
    for i in range(8):
        while True:
            hsh = get_hash(key + str(current_idx))
            current_idx += 1
            if hsh[:5] == '00000':
                code.append(hsh[5])
                break
    return ''.join(code)

def solve_part2(ipt):
    code = [None] * 8
    key = ipt
    current_idx = 0
    while not all(code):
        while True:
            hsh = get_hash(key + str(current_idx))
            current_idx += 1
            if hsh[:5] == '00000' and hsh[5].isdigit() and \
               int(hsh[5]) < len(code) and not code[int(hsh[5])]:
                code[int(hsh [5])] = hsh[6]
                break
    return ''.join(code)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    code = solve_part1(ipt)
    print(code)
    code = solve_part2(ipt)
    print(code)