#!/usr/bin/env python3

from utils import test

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day13.txt'
CARDINALS = [(0, 1), (0, -1), (1, 0), (-1, 0)]


def is_a_wall(x, y, nb):
    formula = x*x + 3*x + 2*x*y + y + y*y
    return sum([int(b) for b in bin(formula + nb)[2:]]) % 2 == 1

def get_next_paths(x, y, nb):
    for direction in CARDINALS:
        new_x, new_y = x + direction[0], y + direction[1]
        if new_x >= 0 and new_y >= 0 and not is_a_wall(new_x, new_y, nb):
            yield (new_x, new_y)

def solve_part1(ipt):
    nb = int(ipt)
    distances = [((1, 1), 0)]
    been = {}
    while distances:
        pos = distances.pop()
        been[pos[0]] = pos[1]
        distances += [(x, pos[1] + 1) for x in get_next_paths(*pos[0], nb)
                      if not x in been or been[x] > pos[1]]

    return been[(31, 39)]

def solve_part2(ipt):
    nb = int(ipt)
    distances = [((1, 1), 0)]
    been = {}
    while distances:
        pos = distances.pop()
        been[pos[0]] = pos[1]
        distances += [(x, pos[1] + 1) for x in get_next_paths(*pos[0], nb)
                      if not x in been or been[x] > pos[1]]

    return len([x for x, v in been.items() if v <= 50])


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_steps = solve_part1(ipt)
    print(nb_steps)
    nb_steps = solve_part2(ipt)
    print(nb_steps)