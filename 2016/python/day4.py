#!/usr/bin/env python3

from utils import test

import re

examples_part1 = {
    ('aaaaa-bbb-z-y-x-123[abxyz]\n'
     'a-b-c-d-e-f-g-h-987[abcde]\n'
     'not-a-real-room-404[oarel]\n'
     'totally-real-room-200[decoy]'): 1514
}

examples_part2 = {
}

INPUT = 'resources/day4.txt'
SECTOR_NAME = [
    'Easter Bunny HQ'.lower(),
    'North Pole'.lower(),
    'NorthPole Object Storage'.lower(),
]


def rotate_name(name, nb):
    new_name = ''
    for letter in name:
        if letter == '-':
            new_name += ' '
        else:
            a, z = ord('a'), ord('z')
            new_name += chr((ord(letter) - a + nb) % (z - a + 1) + a)
    return new_name

def is_valid(name, checksum):
    name = name.replace('-', '')
    distincts = sorted(list(set(name)))
    for i, letter in enumerate(distincts):
        distincts[i] = [letter, name.count(letter)]
    distincts = sorted(distincts, key=lambda x: x[1], reverse=True)
    real_checksum = ''.join([x[0] for x in distincts[:5]])
    return checksum == real_checksum

def solve_part1(ipt):
    all_real_ids = []
    for room in ipt.split():
        name, _id, checksum = re.findall('([a-z\-]+)-(\d+)\[(\w+)\]', room)[0]
        if is_valid(name, checksum):
            all_real_ids.append(int(_id))
    return sum(all_real_ids)

def solve_part2(ipt):
    for room in ipt.split():
        name, _id, checksum = re.findall('([a-z\-]+)-(\d+)\[(\w+)\]', room)[0]
        if is_valid(name, checksum):
            real_name = rotate_name(name, int(_id))
            if real_name in SECTOR_NAME:
                return _id
    return 0


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    sum_real_ids = solve_part1(ipt)
    print(sum_real_ids)
    sum_real_ids = solve_part2(ipt)
    print(sum_real_ids)