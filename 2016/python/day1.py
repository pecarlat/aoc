#!/usr/bin/env python3

from utils import test

examples_part1 = {
    'R2, L3': 5,
    'R2, R2, R2': 2,
    'R5, L5, R5, R3': 12,
}

examples_part2 = {
    'R8, R4, R4, R8': 4,
}

INPUT = 'resources/day1.txt'


def new_orientation(direction, orientation):
    return {
        'R': (orientation + 1) % 4,
        'L': (orientation - 1) % 4,
    }[direction]

def follow_instruction(position, orientation, distance):
    return {
        0: (position[0] - int(distance), position[1]),
        1: (position[0], position[1] + int(distance)),
        2: (position[0] + int(distance), position[1]),
        3: (position[0], position[1] - int(distance)),
    }[orientation]

def trace_instruction(orientation, distance, all_positions):
    sbs = {
        0: (-1, 0),
        1: (0, 1),
        2: (1, 0),
        3: (0, -1),
    }[orientation]
    for i in range(distance):
        curr_pos = all_positions[-1]
        all_positions.append((curr_pos[0] + sbs[0], curr_pos[1] + sbs[1]))
        if all_positions[-1] in all_positions[:-1]:
            return all_positions, True
    return all_positions, False

def solve_part1(ipt):
    instructions = [instruction for instruction in ipt.split(', ')]
    orientation = 0
    position = (0, 0)
    for instruction in instructions:
        direction, distance = instruction[0], int(instruction[1:])
        orientation = new_orientation(direction, orientation)
        position = follow_instruction(position, orientation, distance)
    return sum(map(abs, position))

def solve_part2(ipt):
    instructions = [instruction for instruction in ipt.split(', ')]
    orientation = 0
    all_positions = [(0, 0)]
    for instruction in instructions:
        direction, distance = instruction[0], int(instruction[1:])
        orientation = new_orientation(direction, orientation)
        all_positions, seen_twice = trace_instruction(
            orientation, distance, all_positions)
        if seen_twice:
            break
    return sum(map(abs, all_positions[-1]))


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    distance = solve_part1(ipt)
    print(distance)
    distance = solve_part2(ipt)
    print(distance)