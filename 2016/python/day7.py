#!/usr/bin/env python3

from utils import test

import re

examples_part1 = {
    'abba[mnop]qrst': 1,
    'abcd[bddb]xyyx': 0,
    'aaaa[qwer]tyui': 0,
    'ioxxoj[asdfgh]zxcvbn': 1,
}

examples_part2 = {
    'aba[bab]xyz': 1,
    'xyx[xyx]xyx': 0,
    'aaa[kek]eke': 1,
    'zazbz[bzb]cdb': 1,
}

INPUT = 'resources/day7.txt'


def has_abba(list_seq):
    for seq in list_seq:
        for i in range(len(seq) - 3):
            if seq[i] == seq[i + 3] and \
               seq[i + 1] == seq[i + 2] and \
               seq[i] != seq[i + 1]:
                return True
    return False

def all_aba(list_seq):
    abas = []
    for seq in list_seq:
        for i in range(len(seq) - 2):
            if seq[i] == seq[i + 2] and \
               seq[i] != seq[i + 1]:
                abas.append(seq[i:i + 3])
    return abas

def has_corresponding_bab(list_seq, abas):
    babs = [aba[1:] + aba[1] for aba in abas]
    for seq in list_seq:
        for i in range(len(seq) - 2):
            if seq[i:i + 3] in babs:
                return True
    return False

def is_valid(ip):
    brackets = [s[1:-1] for s in re.findall('[\[].*?[\]]', ip)]
    without_brackets = re.sub('[\[].*?[\]]', ',', ip).split(',')
    return has_abba(without_brackets) and not has_abba(brackets)

def solve_part1(ipt):
    counter = 0
    for ip in ipt.split():
        counter += int(is_valid(ip))
    return counter

def solve_part2(ipt):
    counter = 0
    for ip in ipt.split():
        brackets = [s[1:-1] for s in re.findall('[\[].*?[\]]', ip)]
        without_brackets = re.sub('[\[].*?[\]]', ',', ip).split(',')
        abas = all_aba(without_brackets)
        counter += int(has_corresponding_bab(brackets, abas))
    return counter


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_valid = solve_part1(ipt)
    print(nb_valid)
    nb_valid = solve_part2(ipt)
    print(nb_valid)