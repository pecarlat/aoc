#!/usr/bin/env python3

from utils import test

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day18.txt'
NB_ROWS_1 = 40
NB_ROWS_2 = 400000


def is_trap(e):
    return (not e[0] and not e[1] and e[2]) or \
           (e[0] and not e[1] and not e[2]) or \
           (not e[0] and e[1] and e[2]) or \
           (e[0] and e[1] and not e[2])

def get_next_row(row):
    row = [True] + row + [True]
    for i in range(1, len(row) - 1):
        yield not is_trap(row[i-1:i+2])

def solve_part1(ipt):
    rows = [[c == '.' for c in ipt]]
    for i in range(NB_ROWS_1 - 1):
        rows.append(list(get_next_row(rows[-1])))
    return sum(map(sum, rows))

def solve_part2(ipt):
    rows = [[c == '.' for c in ipt]]
    for i in range(NB_ROWS_2 - 1):
        rows.append(list(get_next_row(rows[-1])))
    return sum(map(sum, rows))


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_safe = solve_part1(ipt)
    print(nb_safe)
    nb_safe = solve_part2(ipt)
    print(nb_safe)