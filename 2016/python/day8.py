#!/usr/bin/env python3

from utils import test

import numpy as np

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day8.txt'
SCREEN_SIZE = (6, 50)


def turn_on(screen, instruction_args):
    loc = instruction_args[0]
    y, x = [int(x) for x in loc.split('x')]
    screen[:x,:y] = 1
    return screen

def rotate(screen, instruction_args):
    index, loc, _, dist = instruction_args
    loc = int(loc.split('=')[1])
    dist = int(dist)
    if index == 'row':
        screen[loc] = np.concatenate([screen[loc][-dist:],
                                      screen[loc][:-dist]])
    else:
        screen[:,loc] = np.concatenate([screen[:,loc][-dist:],
                                        screen[:,loc][:-dist]])
    return screen

def apply_instruction(instruction, screen):
    splitted = instruction.split()
    return {
        'rect': turn_on,
        'rotate': rotate,
    }[splitted[0]](screen, splitted[1:])

def display_capital_letter(letter):
    for i in letter:
        line = ''
        for j in i:
            line += 'o' if j == 1 else ' '
        print(line)
    print('--------------')

def solve_part1(ipt):
    screen = np.zeros(SCREEN_SIZE)
    for instruction in ipt.split('\n'):
        screen = apply_instruction(instruction, screen)
    return np.sum(screen)

def solve_part2(ipt):
    screen = np.zeros(SCREEN_SIZE)
    for instruction in ipt.split('\n'):
        screen = apply_instruction(instruction, screen)
    for i in range(0, 50, 5):
        display_capital_letter(screen[:,i:i+5])
    return 0


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_pixels = solve_part1(ipt)
    print(nb_pixels)
    nb_pixels = solve_part2(ipt)
    print(nb_pixels)