#!/usr/bin/env python3

from utils import test

import math
import hashlib

examples_part1 = {
    'ihgpwlah': 'DDRRRD',
    'kglvqrro': 'DDUDRLRRUDRD',
    'ulqzkmiv': 'DRURDRUDDLLDLUURRDULRLDUUDDDRR',
}

examples_part2 = {
    'ihgpwlah': 370,
    'kglvqrro': 492,
    'ulqzkmiv': 830,
}

INPUT = 'resources/day17.txt'
GRID_SIZE = (4, 4)
VAULT_LOC = (3, 3)


def move(pos, direction):
    return {
        'U': (pos[0] - 1, pos[1]),
        'D': (pos[0] + 1, pos[1]),
        'L': (pos[0], pos[1] - 1),
        'R': (pos[0], pos[1] + 1),
    }[direction]

def get_new_pos(doors, pos, distance, path):
    up, down, left, right = doors
    if up and pos[0] > 0:
        yield [move(pos, 'U'), distance + 1, path + 'U']
    if down and pos[0] < GRID_SIZE[0] - 1:
        yield [move(pos, 'D'), distance + 1, path + 'D']
    if left and pos[1] > 0:
        yield [move(pos, 'L'), distance + 1, path + 'L']
    if right and pos[1] < GRID_SIZE[0] - 1:
        yield [move(pos, 'R'), distance + 1, path + 'R']

def get_doors(key):
    return [s in ['b', 'c', 'd', 'e', 'f'] for s in get_hash(key)[:4]]

def get_hash(key):
    return hashlib.md5(key.encode()).hexdigest()

def solve_part1(ipt):
    positions = [[(0, 0), 0, '']]
    shortest_vault = [math.inf, '']
    while positions:
        pos, distance, path = positions.pop()
        if distance > shortest_vault[0]:
            continue
        if pos == VAULT_LOC:
            shortest_vault = [distance, path]
            continue

        doors = get_doors(ipt + path)
        positions += get_new_pos(doors, pos, distance, path)

    return shortest_vault[1]

def solve_part2(ipt):
    positions = [[(0, 0), 0, '']]
    longest_vault = 0
    while positions:
        pos, distance, path = positions.pop()
        if pos == VAULT_LOC:
            if distance > longest_vault:
                longest_vault = distance
            continue

        doors = get_doors(ipt + path)
        positions += get_new_pos(doors, pos, distance, path)

    return longest_vault


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    path = solve_part1(ipt)
    print(path)
    nb_steps = solve_part2(ipt)
    print(nb_steps)