#!/usr/bin/env python3

from utils import test

from collections import defaultdict

examples_part1 = {
    ('cpy 2 a\n'
     'tgl a\n'
     'tgl a\n'
     'tgl a\n'
     'cpy 1 a\n'
     'dec a\n'
     'dec a'): 3,
}

examples_part2 = {
}

INPUT = 'resources/day23.txt'


def get_int_or_reg(registers, register):
    return registers[register] if register.isalpha() else int(register)

def toggle(instruction):
    action, nb_args = instruction[0], len(instruction)
    if nb_args == 2:
        return (['dec'] if action == 'inc' else ['inc']) + instruction[1:]
    elif nb_args == 3:
        return (['cpy'] if action == 'jnz' else ['jnz']) + instruction[1:]

def unstack_instructions(instructions, registers):
    idx = 0
    while 0 <= idx < len(instructions):
        instruction = instructions[idx]
        action = instruction[0]

        # Hack, bunnies do multiply
        if len(instructions) > 7:
            if idx == 4:
                registers['a'] = registers['b'] * registers['d']
                registers['c'] = 0
                registers['d'] = 0
                idx = 10
                continue

        if action in ['cpy', 'jnz']:
            if len(instruction[1:]) != 2:
                idx += 1
                continue
            value, register = instruction[1:]
            value = get_int_or_reg(registers, value)
            if action == 'cpy':
                registers[register] = value
            elif value != 0:
                idx += get_int_or_reg(registers, register) - 1
        elif action == 'inc' or action == 'dec':
            if len(instruction[1:]) != 1:
                idx += 1
                continue
            register = instruction[1]
            registers[register] += 1 if action == 'inc' else -1
        elif action == 'tgl':
            target = idx + registers[instruction[1]]
            if 0 <= target < len(instructions):
                instructions[target] = toggle(instructions[target])
        idx += 1
    return registers

def solve_part1(ipt):
    instructions = [line.split() for line in ipt.splitlines()]
    registers = defaultdict(int)
    registers['a'] = 7
    registers = unstack_instructions(instructions, registers)
    return registers['a']

def solve_part2(ipt):
    instructions = [line.split() for line in ipt.splitlines()]
    registers = defaultdict(int)
    registers['a'] = 12
    registers = unstack_instructions(instructions, registers)
    return registers['a']


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    register_a = solve_part1(ipt)
    print(register_a)
    register_a = solve_part2(ipt)
    print(register_a)