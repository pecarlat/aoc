#!/usr/bin/env python3

from utils import test

examples_part1 = {
    'ADVENT': 6,
    'A(1x5)BC': 7,
    '(3x3)XYZ': 9,
    'A(2x2)BCD(2x2)EFG': 11,
    '(6x1)(1x3)A': 6,
    'X(8x2)(3x3)ABCY': 18,
}

examples_part2 = {
    '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN': 445,
}

INPUT = 'resources/day9.txt'


def solve_part1(ipt):
    decompressed_size = 0
    while True:
        p1, p2 = ipt.find('('), ipt.find(')')
        if p1 == -1:
            decompressed_size += len(ipt)
            break
        nb_char, nb_repeat = [int(x) for x in ipt[p1 + 1:p2].split('x')]
        decompressed_size += p1 + (int(nb_char) * int(nb_repeat))
        ipt = ipt[p2 + 1 + nb_char:]
    return decompressed_size

def solve_part2(ipt):
    decompressed_size = 0
    weights = [1] * len(ipt)

    while True:
        if ipt[0] == '(':
            p2 = ipt.find(')')
            nb_c, nb_r = [int(x) for x in ipt[1:p2].split('x')]
            n1, n2 = p2 + 1, p2 + 1 + nb_c
            weights = [x * nb_r for x in weights[n1:n2]] + weights[n2:]
            ipt = ipt[n1:]
        else:
            p1 = ipt.find('(')
            if p1 == -1:
                decompressed_size += sum(weights)
                break
            decompressed_size += sum(weights[:p1])
            weights = weights[p1:]
            ipt = ipt[p1:]

    return decompressed_size


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    size = solve_part1(ipt)
    print(size)
    size = solve_part2(ipt)
    print(size)