#!/usr/bin/env python3

from utils import test

import re
from collections import defaultdict

examples_part1 = {
    ('value 61 goes to bot 2\n'
     'bot 2 gives low to bot 1 and high to bot 0\n'
     'value 17 goes to bot 1\n'
     'bot 1 gives low to output 1 and high to bot 0\n'
     'bot 0 gives low to output 2 and high to output 0\n'
     'value 30 goes to bot 2'): 0
}

examples_part2 = {
}

INPUT = 'resources/day10.txt'


def unstack_instructions(instructions):
    bot = defaultdict(list)
    pipeline = {}
    for line in instructions:
        if line.startswith('value'):
            val, bot_nb = map(int, re.findall(r'-?\d+', line))
            bot[bot_nb].append(val)
        else:
            bot_nb, low, high = map(int, re.findall(r'-?\d+', line))
            low_o, high_o = re.findall(r' (bot|output)', line)
            pipeline[bot_nb] = (low_o, low), (high_o, high)
    return bot, pipeline

def solve_part1(ipt):
    output = defaultdict(list)
    instructions = ipt.splitlines()

    bot, pipeline = unstack_instructions(instructions)

    while bot:
        for k, v in dict(bot).items():
            if len(v) == 2:
                v1, v2 = sorted(bot.pop(k))
                if v1 == 17 and v2 == 61:
                    return k

                (t1,n1), (t2,n2) = pipeline[k]
                eval(t1)[n1].append(v1)
                eval(t2)[n2].append(v2)

    return 0

def solve_part2(ipt):
    output = defaultdict(list)
    instructions = ipt.splitlines()

    bot, pipeline = unstack_instructions(instructions)

    while bot:
        for k, v in dict(bot).items():
            if len(v) == 2:
                v1, v2 = sorted(bot.pop(k))
                (t1,n1), (t2,n2) = pipeline[k]
                eval(t1)[n1].append(v1)
                eval(t2)[n2].append(v2)

    a, b, c = (output[k][0] for k in [0, 1, 2])
    return a * b * c


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    bot_number = solve_part1(ipt)
    print(bot_number)
    multiplied = solve_part2(ipt)
    print(multiplied)