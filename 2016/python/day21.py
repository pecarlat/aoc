#!/usr/bin/env python3

from utils import test

from itertools import permutations

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day21.txt'
STARTING_PWD = 'abcdefgh'
FINAL_PWD = 'fbgdceah'


def swap_max(i1, i2):
    if i1 > i2:
        return i2, i1
    return i1, i2

def swap_seq(seq, i1, i2):
    i1, i2 = swap_max(i1, i2)
    return seq[:i1] + seq[i2] + seq[i1+1:i2] + seq[i1] + seq[i2+1:]

def swap(seq, is_position, i1, i2):
    if is_position:
        i1, i2 = int(i1), int(i2)
    else:
        i1, i2 = seq.index(i1), seq.index(i2)
    return swap_seq(seq, i1, i2)

def reverse(seq, i1, i2):
    return seq[:i1] + seq[i1:i2+1][::-1] + seq[i2+1:]

def move(seq, i1, i2):
    to_move = seq[i1]
    if i1 < i2:
        return seq[:i1] + seq[i1+1:i2+1] + to_move + seq[i2+1:]
    else:
        return seq[:i2] + to_move + seq[i2:i1] + seq[i1+1:]

def rotate(seq, i, right):
    if right:
        return seq[len(seq)-i:] + seq[:len(seq)-i]
    else:
        return seq[i:] + seq[:i]

def apply(instruction, seq):
    ins = instruction.split()
    action, detail, elmt1, elmt2 = ins[0], ins[1], ins[2], ins[-1]
    if action == 'swap':
        seq = swap(seq, detail == 'position', elmt1, elmt2)
    elif action == 'reverse':
        seq = reverse(seq, int(elmt1), int(elmt2))
    elif action == 'move':
        seq = move(seq, int(elmt1), int(elmt2))
    elif action == 'rotate':
        if detail in ['left', 'right']:
            seq = rotate(seq, int(elmt1), detail == 'right')
        elif detail == 'based':
            idx = seq.index(elmt2)
            if idx >= 4:
                idx += 1
            seq = rotate(seq, idx + 1, True)
    return seq

def solve_part1(ipt):
    seq = STARTING_PWD
    for line in ipt.splitlines():
        seq = apply(line, seq)
    return seq

def solve_part2(ipt):
    for tmp_seq in [''.join(perm) for perm in permutations(FINAL_PWD)]:
        seq = tmp_seq
        for line in ipt.splitlines():
            seq = apply(line, seq)
        if seq == FINAL_PWD:
            return tmp_seq
    return 'Unknown'


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    password = solve_part1(ipt)
    print(password)
    password = solve_part2(ipt)
    print(password)