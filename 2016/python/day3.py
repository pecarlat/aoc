#!/usr/bin/env python3

from utils import test

import itertools

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day3.txt'


def is_valid(s1, s2, s3):
    return s1 + s2 > s3 and \
           s1 + s3 > s2 and \
           s2 + s3 > s1

def solve_part1(ipt):
    counter = 0
    triangles = [triangle.split() for triangle in ipt.split('\n')]
    for triangle in triangles:
        counter += int(is_valid(*[int(side) for side in triangle]))
    return counter

def solve_part2(ipt):
    counter = 0
    triangles = [triangle.split() for triangle in ipt.split('\n')]
    for i in range(0, len(triangles), 3):
        for j in range(3):
            sides = [triangles[i][j], triangles[i + 1][j], triangles[i + 2][j]]
            counter += int(is_valid(*[int(side) for side in sides]))
    return counter


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    nb_valid = solve_part1(ipt)
    print(nb_valid)
    nb_valid = solve_part2(ipt)
    print(nb_valid)