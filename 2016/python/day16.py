#!/usr/bin/env python3

from utils import test

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day16.txt'
LENGTH_1 = 272
LENGTH_2 = 35651584


def step_increase(seq):
    rev_seq = [int(not bool(s)) for s in seq[::-1]]
    return seq + [0] + rev_seq

def step_decrease(seq):
    for i in range(0, len(seq), 2):
        yield int(seq[i] == seq[i+1])

def increase_until(seq, length):
    while len(seq) < length:
        seq = step_increase(seq)
    return seq[:length]

def solve_part1(ipt):
    seq = [int(s) for s in ipt]
    seq = increase_until(seq, LENGTH_1)
    while len(seq) % 2 != 1:
        seq = list(step_decrease(seq))
    return ''.join([str(s) for s in seq])

def solve_part2(ipt):
    seq = [int(s) for s in ipt]
    seq = increase_until(seq, LENGTH_2)
    while len(seq) % 2 != 1:
        seq = list(step_decrease(seq))
    return ''.join([str(s) for s in seq])


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    checksum = solve_part1(ipt)
    print(checksum)
    checksum = solve_part2(ipt)
    print(checksum)