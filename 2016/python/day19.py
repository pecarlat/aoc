#!/usr/bin/env python3

from utils import test

import numpy as np

examples_part1 = {
    '5': 3,
}

examples_part2 = {
    '5': 2,
}

INPUT = 'resources/day19.txt'


def steal_and_find(elves):
    if len(elves) <= 2:
        return elves[0]

    if len(elves) % 2:
        return steal_and_find(np.roll(elves[::2], 1))
    else:
        return steal_and_find(elves[::2])

def steal_in_front_and_find(elves):
    if len(elves) <= 2:
        return elves[0]

    across = len(elves) // 2
    if len(elves) % 2:
        removed = len(elves) - (len(elves) + 1) // 3
        new = np.roll(elves, -across)[1::3]
    else:
        removed = len(elves) - len(elves) // 3
        new = np.roll(elves, -across)[2::3]

    return steal_in_front_and_find(np.roll(new, across - removed))

def solve_part1(ipt):
    nb_elves = int(ipt)
    elves = np.arange(nb_elves) + 1
    return steal_and_find(elves)

def solve_part2(ipt):
    nb_elves = int(ipt)
    elves = np.arange(nb_elves) + 1
    return steal_in_front_and_find(elves)


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    elf_position = solve_part1(ipt)
    print(elf_position)
    elf_position = solve_part2(ipt)
    print(elf_position)