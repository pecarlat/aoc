#!/usr/bin/env python3

from utils import test

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day15.txt'


def good_config(time, discs):
    for i, (nb_pos, init_pos) in enumerate(discs):
        if (init_pos + time + i + 1) % nb_pos != 0:
            return False
    return True

def get_disc(line):
    splitted = line.split()
    return int(splitted[3]), int(splitted[-1][:-1])

def solve_part1(ipt):
    discs = list(map(get_disc, ipt.splitlines()))
    time = 0
    while not good_config(time, discs):
        time += 1
    return time

def solve_part2(ipt):
    discs = list(map(get_disc, ipt.splitlines())) + [(11, 0)]
    time = 0
    while not good_config(time, discs):
        time += 1
    return time


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    first_time = solve_part1(ipt)
    print(first_time)
    first_time = solve_part2(ipt)
    print(first_time)