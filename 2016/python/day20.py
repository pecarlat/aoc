#!/usr/bin/env python3

from utils import test

examples_part1 = {
}

examples_part2 = {
}

INPUT = 'resources/day20.txt'


def get_ips(ipt):
    for line in ipt.splitlines():
        low, high = map(int, line.split('-'))
        yield (low, high)

def solve_part1(ipt):
    ips = sorted(get_ips(ipt), key=lambda x: x[0])
    used = 0
    for low, high in ips:
        if low <= used + 1:
            used = max(used, high)
        else:
            return used + 1

    return used

def solve_part2(ipt):
    ips = sorted(get_ips(ipt), key=lambda x: x[0])
    used, count = 0, 0
    for low, high in ips:
        if low > used + 1:
            count += low - used - 1
        used = max(used, high)
    count += 2**32 - 1 - used

    return count


if __name__ == '__main__':

    test("part1", examples_part1, solve_part1)
    test("part2", examples_part2, solve_part2)

    # Reader
    ipt = open(INPUT).read()
    lowest_ip = solve_part1(ipt)
    print(lowest_ip)
    lowest_ip = solve_part2(ipt)
    print(lowest_ip)